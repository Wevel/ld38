﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent (typeof (Map), typeof (ItemManager))]
public class GameControl : MonoBehaviour
{
	public int seed;
	public int mapSize;

	public string gameName;

	public Map map { get; private set; }
	public ItemManager itemManager { get; private set; }
	public Bank bank { get; private set; }
	public Tutorial tutorial { get; private set; }

	public LineRenderer2D outputLineBuildingDisplay;
	public LineRenderer2D outputLineBuildingDisplayHighlight;
	public ShapeRenderer currentBuildingTypeShape;
	public PolygonCollider2D currentBuildingTypeCollider;

	public Material validOutputLineMaterial;
	public Material invalidOutputLineMaterial;

	public Material validOutputLineHighlightMaterial;
	public Material invalidOutputLineHighlightMaterial;

	public Building currentBuilding { get; private set; }

	public TransportLine currentTransportLine;

	public  BuildingType currentBuildingType { get; private set; }

	//private Stack<GameControlState> state;

	public uint currentTransportLineCost { get; private set; }
	public bool isCreatingOutputLine { get; private set; }

	private void Awake ()
	{
		map = this.GetComponent<Map> ();
		itemManager = this.GetComponent<ItemManager> ();
		bank = this.GetComponent<Bank> ();
		tutorial = this.GetComponent<Tutorial> ();

		Debug.Assert (map != null, "GameControl.Awake: There needs to be a map on this gameobject", this);

		DataManager.Setup (itemManager, bank);
	}

	private void Start ()
	{
		map.Generate ();

		if (MainMenu.Instance.playTutorial) tutorial.StartTutorial ();
	}

	private void Update ()
	{
		if (map != null)
		{
			if (!MainMenu.Instance.paused)
			{
				// Make sure that mouse is on the screen
				if (Camera.main.pixelRect.Contains (Input.mousePosition))
				{
					Vector2 currentPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);

					if (currentBuildingType != null)
					{
						outputLineBuildingDisplay.gameObject.SetActive (false);

						currentBuildingType.SetupShapeRenderer (currentBuildingTypeShape);
						currentBuildingType.SetupPolygonCollider (currentBuildingTypeCollider);
						currentBuildingTypeShape.transform.position = currentPoint;
						currentBuildingTypeShape.gameObject.SetActive (true);

						if (map.CanPlaceBuilding (currentPoint, currentBuildingType)) currentBuildingTypeShape.colour = map.normalRunningBuildingColour;
						else currentBuildingTypeShape.colour = map.needInputBuildingColour;
					}
					else
					{
						currentBuildingTypeShape.gameObject.SetActive (false);

						if (currentBuilding != null)
						{
							if (isCreatingOutputLine)
							{
								Building otherBuilding = map.GetBuilding (currentPoint);

								List<Vector2> points;
								List<TransportLine> overlapping;
								bool isValid = false;

								if (otherBuilding != null)
								{
									uint cost;
									isValid = map.CanBuildOutputLine (currentBuilding, otherBuilding, out points, out overlapping, out cost);
									currentTransportLineCost = cost;
								}
								else
								{
									points = map.GetTransportLinePoints (currentBuilding.position, currentPoint, currentBuilding.type.size, currentBuilding.type.size, false);
									isValid = false;

									currentTransportLineCost = Bank.GetNewTransportLineCost (points);
								}

								map.SetupLineRenderer (outputLineBuildingDisplay, points);
								map.SetupLineRenderer (outputLineBuildingDisplayHighlight, points);



								if (isValid)
								{
									outputLineBuildingDisplay.material = validOutputLineMaterial;
									outputLineBuildingDisplayHighlight.material = validOutputLineHighlightMaterial;
								}
								else
								{
									outputLineBuildingDisplay.material = invalidOutputLineMaterial;
									outputLineBuildingDisplayHighlight.material = invalidOutputLineHighlightMaterial;
								}
							}
						}
						else
						{
							outputLineBuildingDisplay.gameObject.SetActive (false);
						}
					}

					if (!EventSystem.current.IsPointerOverGameObject ())
					{
						if (Input.GetMouseButtonDown (0))
						{
							if (currentBuildingType != null)
							{
								if (map.TryPlaceBuilding (currentPoint, currentBuildingType))
								{
									currentBuildingType = null;
								}
								else
								{
									Debug.Log ("GameControl.Update: Can't build there");
								}
							}
							else if (currentBuilding != null)
							{
								Building otherBuilding = map.GetBuilding (currentPoint);

								if (otherBuilding != null)
								{
									if (isCreatingOutputLine)
									{
										if (map.TryBuildOutputLine (currentBuilding, otherBuilding))
										{
											isCreatingOutputLine = false;

											outputLineBuildingDisplay.gameObject.SetActive (false);
										}
										else
										{
											Debug.Log ("GameControl.Update: Can't build output line");
										}
									}
									else
									{
										currentBuilding = otherBuilding;
									}
								}
								else
								{
									currentBuilding = null;
									currentTransportLine = map.GetTransportLine (currentPoint);
								}
							}
							else
							{
								currentBuilding = map.GetBuilding (currentPoint);

								if (currentBuilding == null) currentTransportLine = map.GetTransportLine (currentPoint);
							}
						}
						else if (Input.GetMouseButtonDown (1))
						{
							currentBuildingTypeShape.gameObject.SetActive (false);
							outputLineBuildingDisplay.gameObject.SetActive (false);
							currentBuilding = null;
							currentBuildingType = null;
							isCreatingOutputLine = false;
						}
					}
				}
				else
				{
					currentBuildingTypeShape.gameObject.SetActive (false);
				}
			}
		}
		else
		{
			currentBuildingTypeShape.gameObject.SetActive (false);
			outputLineBuildingDisplay.gameObject.SetActive (false);
			currentBuilding = null;
			//state = GameControlState.Initial;
		}
	}

	public void SetBuildingType (string name)
	{
		currentBuildingType = DataManager.GetBuildingType (name);
		currentBuilding = null;

		if (currentBuildingType == null) Debug.Log ("No building with name: " + name);
		//else state = GameControlState.PlaceBuilding;
	}

	public void StartOutputLine ()
	{
		isCreatingOutputLine = true;
	}

	public void DestroyCurrent ()
	{
		if (currentBuilding != null) map.DestroyBuilding (currentBuilding);
		else if (currentTransportLine != null) map.DestroyTransportLine (currentTransportLine);
	}

	private void OnDrawGizmos ()
	{
		if (currentBuilding != null)
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere (currentBuilding.position, currentBuilding.type.size);
		}
	}
}

﻿public enum GameControlState
{
	Initial,
	PlaceBuilding,
	BuildingSelected,
	PlaceOutputLine,
}
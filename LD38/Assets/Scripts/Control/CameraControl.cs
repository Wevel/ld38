﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public float moveSpeed = 5;
	public float zoomeSpeed = 1;

	public float minZoom = 2;
	public float maxZoom = 40;

	public bool canMove = true;

	Vector3 lastPosition;
	
	void Update () {
		if (canMove && !MainMenu.Instance.paused)
		{
			Camera camera = Camera.main;

			if (camera != null)
			{
				// Make sure the mouse is on the screen
				if (camera.pixelRect.Contains (Input.mousePosition))
				{
					// Moving the camera
					if (Input.GetMouseButtonDown (2)) lastPosition = camera.ScreenToWorldPoint (Input.mousePosition);

					Vector3 mousePosition = camera.ScreenToWorldPoint (Input.mousePosition);

					if (Input.GetMouseButton (2)) camera.transform.Translate (lastPosition - mousePosition);
					lastPosition = camera.ScreenToWorldPoint (Input.mousePosition);

					// Zooming
					camera.orthographicSize -= camera.orthographicSize * Input.GetAxis ("Mouse ScrollWheel") * zoomeSpeed;
					camera.orthographicSize = Mathf.Clamp (camera.orthographicSize, minZoom, maxZoom);
				}
			}
		}
	}
}

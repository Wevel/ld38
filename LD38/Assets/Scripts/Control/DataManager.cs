﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataManager
{
	public const int BuildingLayer = 8;
	public const int TransportLayer = 9;
	public const int ItemLayer = 10;

	public static ItemManager ItemManager { get; private set; }
	public static Bank Bank { get; private set; }

	private static Dictionary<string, BuildingType> possibleBuildings = new Dictionary<string, BuildingType> ();
	private static Dictionary<string, ItemType> possibleItems = new Dictionary<string, ItemType> ();
	private static Dictionary<string, Recipe> possibleRecipes = new Dictionary<string, Recipe> ();

	public static IEnumerable<BuildingType> PossibleBuildings
	{
		get
		{
			foreach (KeyValuePair<string, BuildingType> item in possibleBuildings) yield return item.Value;
		}
	}

	public static IEnumerable<ItemType> PossibleItems
	{
		get
		{
			foreach (KeyValuePair<string, ItemType> item in possibleItems) yield return item.Value;
		}
	}

	/// <summary>
	/// Returns a building type that has been defined with the given name
	/// </summary>
	/// <param name="name">The name of the building type to find</param>
	/// <returns></returns>
	public static BuildingType GetBuildingType (string name)
	{
		if (possibleBuildings.ContainsKey (name)) return possibleBuildings[name];
		return null;
	}

	public static ItemType GetItemType (string name)
	{
		if (possibleItems.ContainsKey (name)) return possibleItems[name];
		return null;
	}

	public static Recipe GetRecpeType (string name)
	{
		if (possibleRecipes.ContainsKey (name)) return possibleRecipes[name];
		return null;
	}

	public static List<ItemType> GetOreItems ()
	{
		List<ItemType> items = new List<ItemType> ();

		foreach (KeyValuePair<string, ItemType> item in possibleItems) if (item.Value.isOre) items.Add (item.Value);

		return items;
	}

	public static List<string> GetPossibleRecipies (BuildingType type)
	{
		List<string> possible = new List<string> ();

		if (type.name == "SpacePort")
		{
			foreach (KeyValuePair<string, Recipe> item in possibleRecipes) if (item.Key.Contains ("Sell")) possible.Add (item.Value.name);
		}
		else if (type.type == ShapeMeshType.Basic)
		{
			foreach (KeyValuePair<string, Recipe> item in possibleRecipes) if (!item.Key.Contains("Sell") && !item.Value.hasStaredOutput) possible.Add (item.Value.name);
		}
		else if (type.type == ShapeMeshType.Star)
		{
			foreach (KeyValuePair<string, Recipe> item in possibleRecipes) if (!item.Key.Contains ("Sell") && item.Value.hasStaredOutput) possible.Add (item.Value.name);
		}

		return possible;
	}

	public static void Setup (ItemManager itemManager, Bank bank)
	{
		ItemManager = itemManager;
		Bank = bank;

		possibleBuildings.Clear ();
		possibleItems.Clear ();
		possibleRecipes.Clear ();

		#region Buildings

		Dictionary<Building, string> resourceMiningTypes = new Dictionary<Building, string> ();

		addBuilding ("Mine", 0.5f, 0.5f, 3, 500, ShapeMeshType.Basic, 0, false,
			(building, deltaTime) =>
			{
				if (!resourceMiningTypes.ContainsKey (building))
				{
					ResourceSpawn resource = building.map.GetResourceArea (building.position);
					if (resource == null) throw new System.Exception ("DataManager.Setup: Mine was able to be placed but didn't end up having a resource spawn under it");

					resourceMiningTypes.Add (building, resource.type.name);
				}

				itemManager.UpdateResourceCollect (building, deltaTime, resourceMiningTypes[building]);
			},
			(map, position, type) =>
			{
				return defaultBuildingBuildTest (map, position, type) && map.GetResourceArea (position) != null;
			});

		addBuilding ("Refiner", 0.5f, 0.5f, 4, 1000, ShapeMeshType.Basic, 0, true,
			(building, deltaTime) =>
			{
				itemManager.UpdateItemCraft (building, deltaTime);
			},
			(map, position, type) =>
			{
				return defaultBuildingBuildTest (map, position, type);				
			});

		addBuilding ("Crafter", 0.75f, 0.5f, 4, 1750, ShapeMeshType.Star, 0, true,
			(building, deltaTime) =>
			{
				itemManager.UpdateItemCraft (building, deltaTime);
			},
			(map, position, type) =>
			{
				return defaultBuildingBuildTest (map, position, type);
			});

		Dictionary<Building, List<ITransportConnectable>> invalidOutputLines = new Dictionary<Building, List<ITransportConnectable>> ();

		addBuilding ("Warehouse", 0.5f, 0.5f, 32, 1000, ShapeMeshType.Basic, 25, false,
			(building, deltaTime) =>
			{
				if (!invalidOutputLines.ContainsKey (building)) invalidOutputLines.Add (building, new List<ITransportConnectable> ());

				uint count;
				ITransportConnectable destination;

				// Going to need to look to see if anthing is supost to be outputted
				foreach (TransportLine item in building.transportInterface.Outputs)
				{
					foreach (ItemType type in building.transportInterface.StoredTypes)
					{
						if (item.end.IsItemNeeded (new List<ITransportConnectable>(), type, out destination, out count) && count > 0)
						{
							if (!invalidOutputLines[building].Contains (destination))
							{
								building.transportInterface.OutputFromStorage (type.name, count, item, (a) => { invalidOutputLines[building].Remove (destination); }, null, destination);
								invalidOutputLines[building].Add (destination);
								return;
							}
						}
					}
				}
			},
			(map, position, type) =>
			{
				return defaultBuildingBuildTest (map, position, type);
			});

		addBuilding ("SpacePort", 0.75f, 0.5f, 8, 0, ShapeMeshType.Star, 0, true,
			(building, deltaTime) =>
			{
				if (!invalidOutputLines.ContainsKey (building)) invalidOutputLines.Add (building, new List<ITransportConnectable> ());

				// Tick the craft for the recipe
				itemManager.UpdateItemCraft (building, deltaTime);

				uint count;
				ITransportConnectable destination;

				// Going to need to look to see if anthing is supost to be outputted
				foreach (TransportLine item in building.transportInterface.Outputs)
				{
					foreach (KeyValuePair<string, ItemType> type in possibleItems)
					{
						if (item.end.IsItemNeeded (new List<ITransportConnectable> (), type.Value, out destination, out count))
						{
							if (!invalidOutputLines[building].Contains (destination))
							{
								bank.TryBuy (building, type.Value, count, item, (a) => { invalidOutputLines[building].Remove (destination); }, destination);
								invalidOutputLines[building].Add (destination);
								return;
							}
						}
					}
				}
			},
			(map, position, type) =>
			{
				return defaultBuildingBuildTest (map, position, type);
			},
			bank.FinishSellCraft);

		#endregion
		#region Items

		addItem ("Iron-Ore", 0.2f, 3, ShapeMeshType.Basic, 25, new Color32 (114, 45, 0, 255), true);
		addItem ("Copper-Ore", 0.2f, 3, ShapeMeshType.Basic, 35, new Color32 (60, 140, 110, 255), true);
		addItem ("Iron-Plate", 0.2f, 4, ShapeMeshType.Basic, 65, new Color32 (89, 96, 106, 255), false);
		addItem ("Steel-Plate", 0.2f, 5, ShapeMeshType.Basic, 225, new Color32 (168, 194, 195, 255), false);
		addItem ("Copper-Plate", 0.2f, 4, ShapeMeshType.Basic, 85, new Color32 (153, 90, 0, 255), false);
		addItem ("Circuit", 0.2f, 5, ShapeMeshType.Star, 275, new Color32 (14, 101, 0, 255), false);
		addItem ("Electric-Motor", 0.2f, 6, ShapeMeshType.Star, 450, new Color32 (14, 101, 0, 255), false);
		addItem ("Stucture", 0.2f, 6, ShapeMeshType.Star, 425, new Color32 (0, 187, 194, 255), false);
		addItem ("Satellite", 0.2f, 7, ShapeMeshType.Star, 1500, new Color32 (153, 0, 11, 255), false);

		#endregion
		#region Recipes
		#region Refine

		addRecipe ("Smelt-Iron-Ore", 1.5f, 
			new string[] 
			{
				"Iron-Ore",
			},
			new string[]
			{
				"Iron-Plate",
			});

		addRecipe ("Smelt-Copper-Ore", 1.5f,
			new string[]
			{
				"Copper-Ore",
			},
			new string[]
			{
				"Copper-Plate",
			});

		addRecipe ("Smelt-Steel-Plate", 2.5f,
			new string[]
			{
				"Iron-Plate",
				"Iron-Plate",
			},
			new string[]
			{
				"Steel-Plate",
			});

		#endregion

		addRecipe ("Build-Circuit", 1f,
			new string[]
			{
				"Copper-Plate",
				"Copper-Plate",
			},
			new string[]
			{
				"Circuit",
			});

		addRecipe ("Build-Electric-Motor", 3.0f,
			new string[]
			{
				"Circuit",
				"Iron-Plate",
			},
			new string[]
			{
				"Electric-Motor",
			});

		addRecipe ("Build-Stucture", 4f,
			new string[]
			{
				"Steel-Plate",
				"Copper-Plate",
			},
			new string[]
			{
				"Stucture",
			});

		addRecipe ("Build-Satellite", 5.5f,
			new string[]
			{
				"Stucture",
				"Electric-Motor",
				"Circuit",
			},
			new string[]
			{
				"Satellite",
			});

		#region Sell Recipes

		foreach (KeyValuePair<string, ItemType> item in possibleItems)
		{
			addRecipe (
				"Sell-" + item.Key, 
				0,
				new string[]
				{
					item.Key,
				},
				new string[0]);
		}

		#endregion

		#endregion
	}

	private static void addBuilding (string name, float size, float wallPercentThichness, int sides, uint cost, ShapeMeshType type, int internalStorageSize, bool needsRecipe, BuildingUpdate onUpdate, BuildingBuildTest onBuildTest, BuildingFinishCraft onFinishCraft = null)
	{
		if (possibleBuildings.ContainsKey (name)) throw new System.Exception ("BuildingManager.addBuilding: Already a building with name - " + name);

		possibleBuildings.Add (name, new BuildingType (name, size, wallPercentThichness, sides, cost, type, internalStorageSize, needsRecipe, onUpdate, onBuildTest, onFinishCraft));
	}
	
	private static void addItem (string name, float size, int sides, ShapeMeshType type, uint basePrice, Color32 colour, bool isOre)
	{
		if (possibleItems.ContainsKey (name)) throw new System.Exception ("BuildingManager.addItem: Already an item with name - " + name);

		possibleItems.Add (name, new ItemType (name, size, sides, type, basePrice, colour, isOre));
	}

	private static void addRecipe (string name, float craftTime, string[] inputs, string[] outputs)
	{
		if (possibleRecipes.ContainsKey (name)) throw new System.Exception ("BuildingManager.addRecipe: Already a recipe with name - " + name);

		possibleRecipes.Add (name, new Recipe (name, craftTime, inputs, outputs));
	}

	private static bool defaultBuildingBuildTest (Map map, Vector2 position, BuildingType type)
	{
		// Make sure the point in on the map
		if (!map.OnMap (position, type.size)) return false;

		// Make sure that building doesn't overlap another one
		if (map.IsBuildCheckCollision ()) return false;

		return true;
	}
}

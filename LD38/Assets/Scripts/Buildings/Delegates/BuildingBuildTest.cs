﻿using UnityEngine;

public delegate bool BuildingBuildTest (Map map, Vector2 position, BuildingType type);
﻿/// <summary>
/// Called when updateing a building get given a building and a deltaTime
/// </summary>
/// <param name="building"></param>
/// <param name="deltaTime"></param>
public delegate void BuildingUpdate (Building building, float deltaTime);
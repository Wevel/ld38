﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void BuildingFinishCraft (Building building, Recipe recipe);
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour, ITransportConnectable
{
	public BuildingType type { get; private set; }
	public TransportInterface transportInterface { get; private set; }

	public Recipe recipe { get; private set; }

	public PolygonCollider2D polygonCollider { get; private set; }

	public Map map { get; private set; }

	private ShapeRenderer shapeRenderer;

	public Vector2 position
	{
		get
		{
			return transform.position;
		}
	}

	public bool CanCraft
	{
		get
		{
			if (type.needsRecipe) return recipe != null && (transportInterface.OutputQueueSize < recipe.totalOutputs || recipe.totalOutputs == 0) && recipe.HasInputs (this);
			else return transportInterface.OutputQueueSize <= 1;
		}
	}

	private void Awake ()
	{
		shapeRenderer = this.GetComponent<ShapeRenderer> ();

		transportInterface = new TransportInterface (this);
	}

	private void Update ()
	{
		if (!MainMenu.Instance.paused) transportInterface.UpdateOutputs ();

		// Set material correctly
		if (type.needsRecipe && recipe == null) shapeRenderer.colour = map.needRecipeBuildingColour;
		else if ((transportInterface.OutputQueueSize > 0 && !transportInterface.CanOutput) || (type.internalStorageSize > 0 && transportInterface.TotalStoredCount >= type.internalStorageSize)) shapeRenderer.colour = map.cantOutputBuildingColour;
		else if (type.needsRecipe && !recipe.HasInputs (this)) shapeRenderer.colour = map.needInputBuildingColour;
		else shapeRenderer.colour = map.normalRunningBuildingColour;
	}

	public void Setup (Map map,BuildingType type, PolygonCollider2D polygonCollider)
	{
		if (this.type != null) throw new System.Exception ("Building.Setup: Building type already set");
		this.map = map;
		this.type = type;
		this.polygonCollider = polygonCollider;
	}

	public void SetRecipe (string name)
	{
		if (!type.needsRecipe) throw new System.Exception ("Building.SetRecipe: Building " + type.name + " doesn't need a recipe set");

		Recipe r;

		if (name != "")
		{
			r = DataManager.GetRecpeType (name);
			if (r == null) throw new System.Exception ("Building.SetRecipe: Recipe not found with name - " + name);
		}
		else
		{
			r = null;
		}

		// Make sure that the recipe is changed
		if (r != recipe)
		{
			// Set the new recipe
			recipe = r;

			// Cycle the items so that they get added to the building again
			transportInterface.CycleItems ();
		}
	}

	public bool TryGiveItem (Item item, ItemArrived onArrive, ShapeRenderer display, ITransportConnectable destination)
	{
		if (!type.needsRecipe)
		{
			// If there is space in storage in the building add the item to storage rather than to the output queue
			if (transportInterface.TotalStoredCount < type.internalStorageSize) transportInterface.AddToStorage (item.type);
			else if (transportInterface.OutputCount > 0 && type.internalStorageSize == 0 && (transportInterface.CanOutput || transportInterface.OutputQueueSize <= 1)) transportInterface.AddToOutputQueue (item.type);
			else return false;

			if (onArrive != null) onArrive (item);
			DataManager.ItemManager.ReleaseItemDisplay (display);
			return true;
		}

		if (recipe == null) return false;

		uint count;
		if (recipe.IsRequired (item.type.name, out count))
		{
			uint currentCount = transportInterface.StoredCount (item.type.name);

			if (currentCount < count * 2 || transportInterface.TotalStoredCount < type.internalStorageSize)
			{
				transportInterface.AddToStorage (item.type);
				if (onArrive != null) onArrive (item);
				DataManager.ItemManager.ReleaseItemDisplay (display);
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (transportInterface.OutputCount > 0)
			{
				// If there is space in storage in the building add the item to storage rather than to the output queue
				if (transportInterface.TotalStoredCount < type.internalStorageSize) transportInterface.AddToStorage (item.type);
				else if (transportInterface.CanOutput || transportInterface.OutputQueueSize <= 1) transportInterface.AddToOutputQueue (item.type);
				else return false;

				if (onArrive != null) onArrive (item);
				DataManager.ItemManager.ReleaseItemDisplay (display);
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public bool IsItemNeeded (List<ITransportConnectable> previouseNodes, ItemType item, out ITransportConnectable destination)
	{
		destination = null;
		if (previouseNodes.Contains (this)) return false;
		if (type.needsRecipe)
		{
			if (recipe == null) return false;
			return recipe.IsRequired (this, item.name);
		}
		else
		{
			if (type.internalStorageSize == 0) return false;
			previouseNodes.Add (this);
			foreach (TransportLine output in transportInterface.Outputs) if (output.end.IsItemNeeded (previouseNodes, item, out destination)) return true;
			if (transportInterface.TotalStoredCount < type.internalStorageSize) return true;
			return false;
		}
	}

	public bool IsItemNeeded (List<ITransportConnectable> previouseNodes, ItemType item, out ITransportConnectable destination, out uint count)
	{
		count = 0;
		destination = null;
		if (previouseNodes.Contains (this)) return false;
		if (type.needsRecipe)
		{
			if (recipe == null) return false;
			return recipe.IsRequired (this, item.name, out count);
		}
		else
		{
			if (type.internalStorageSize == 0) return false;
			previouseNodes.Add (this);
			foreach (TransportLine output in transportInterface.Outputs) if (output.end.IsItemNeeded (previouseNodes, item, out destination, out count)) return true;
			if (transportInterface.TotalStoredCount < type.internalStorageSize)
			{
				count = 1;
				return true;
			}
			return false;
		}
	}

	public override string ToString ()
	{
		return "Building_" + type.name + ":" + position.ToString (); 
	}
}

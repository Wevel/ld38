﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingType
{	
	public string name { get; private set; }
	public float size { get; private set; }
	public float wallPercentThickness { get; private set; }
	public int sides { get; private set; }
	public int internalStorageSize { get; private set; }
	public bool needsRecipe { get; private set; }
	public uint cost { get; private set; }
	public ShapeMeshType type { get; private set; }
	public BuildingUpdate onUpdate { get; private set; }
	public BuildingBuildTest onBuildTest { get; private set; }
	public BuildingFinishCraft onFinishCraft { get; private set; }

	private Vector2[] outlinePoints = null;

	public BuildingType (string name, float size, float wallPercentThickness, int sides, uint cost, ShapeMeshType type, int internalStorageSize, bool needsRecipe, BuildingUpdate onUpdate, BuildingBuildTest onBuildTest, BuildingFinishCraft onFinishCraft)
	{
		this.name = name;
		this.size = size;
		this.wallPercentThickness = wallPercentThickness;
		this.sides = sides;
		this.cost = cost;
		this.type = type;
		this.internalStorageSize = internalStorageSize;
		this.needsRecipe = needsRecipe;

		this.onUpdate = onUpdate;
		this.onBuildTest = onBuildTest;
		this.onFinishCraft = onFinishCraft;
	}

	public void SetupShapeRenderer (ShapeRenderer shape)
	{
		shape.size = size;
		shape.sides = sides;
		shape.type = type;
		shape.outline = true;
		shape.wallPercentThickness = wallPercentThickness;
		shape.Regenerate ();
	}

	public void SetupPolygonCollider (PolygonCollider2D polygonCollider)
	{
		if (outlinePoints == null) outlinePoints = ShapeGeneration.GenerateShapeOutline (size, sides, 90f - (180f / sides), type, wallPercentThickness).ToArray ();
		polygonCollider.points = outlinePoints;
	}
}

﻿using System.Collections.Generic;
using UnityEngine;

public static class ShapeGeneration
{
	/// <summary>
	/// Generates a filled in mesh for the shape
	/// </summary>
	/// <param name="size"></param>
	/// <param name="sides"></param>
	/// <param name="type"></param>
	/// <returns></returns>
	public static Mesh GenerateShapeMesh (float size, int sides, float baseRotation, ShapeMeshType type)
	{
		if (size < 0) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeMesh: Size must be greater than 0");
		if (sides < 3) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeMesh: Size must be at least 3");

		List<Vector2> outlinePoints = new List<Vector2> ();

		#region Generate Outline Points

		float angleStep = 360f / sides;

		switch (type)
		{
			case ShapeMeshType.Basic:
				for (int i = 0; i < sides; i++) outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
				break;
			case ShapeMeshType.Star:
				float indent = getStarIndentPercent (sides) * size;

				for (int i = 0; i < sides; i++)
				{
					outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
					outlinePoints.Add (Quaternion.Euler (0, 0, (i + 0.5f) * angleStep - baseRotation) * new Vector2 (indent, 0));
				}
				break;
			default:
				throw new System.Exception ("ShapeGeneration.GenerateShapeMesh: Unknown shape type - " + type);
		}

		#endregion

		Mesh mesh = new Mesh ();

		#region Generate Mesh

		// Generate verts
		Vector3[] verts = new Vector3[outlinePoints.Count + 1];
		verts[0] = Vector3.zero;

		for (int i = 0; i < outlinePoints.Count; i++) verts[i + 1] = outlinePoints[i];

		// Generate tris
		int[] tris = new int[outlinePoints.Count * 3];

		for (int i = 0; i < outlinePoints.Count; i++)
		{
			tris[(i * 3) + 0] = 0;
			tris[(i * 3) + 1] = i + 1;
			tris[(i * 3) + 2] = ((i + 1) % outlinePoints.Count) + 1;
		}

		// Generate uvs
		Vector2[] uvs = new Vector2[verts.Length];

		Vector2 centre = Vector2.zero;
		float left = 0;
		float right = 0;
		float top = 0;
		float bottom = 0;

		for (int i = 1; i < verts.Length; i++)
		{
			centre += (Vector2)verts[i];

			if (verts[i].x > right) right = verts[i].x;
			else if (verts[i].x < left) left = verts[i].x;

			if (verts[i].y > top) top = verts[i].x;
			else if (verts[i].y < bottom) bottom = verts[i].x;
		}

		// Get average centre
		centre /= verts.Length;
		float width = right - left;
		float height = top - bottom;

		for (int i = 0; i < uvs.Length; i++)
		{
			uvs[i] = ((Vector2)verts[i] - centre);
			uvs[i].x /= width;
			uvs[i].y /= height;
		}

		// Generate normals
		Vector3[] normals = new Vector3[verts.Length];
		for (int i = 0; i < normals.Length; i++) normals[i] = Vector3.forward;

		// Generate mesh from data
		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.uv = uvs;
		mesh.normals = normals;

		#endregion

		return mesh;
	}

	/// <summary>
	/// Generates a mesh for a shape that is just the outline
	/// </summary>
	/// <param name="size"></param>
	/// <param name="sides"></param>
	/// <param name="type"></param>
	/// <param name="wallThickness"></param>
	/// <returns></returns>
	public static Mesh GenerateShapeMesh (float size, int sides, float baseRotation, ShapeMeshType type, float wallPercentThickness)
	{
		if (size < 0) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeMesh: Size must be greater than 0");
		if (sides < 3) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeMesh: Size must be at least 3");
		if (wallPercentThickness < 0 || wallPercentThickness > 1) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeMesh: Wall percent thickness must be between 0 and 1");

		List<Vector2> outlinePoints = new List<Vector2> ();
		List<Vector2> insidePoints = new List<Vector2> ();

		#region Generate Outline Points

		float angleStep = 360f / sides;
		//float offsetAngle = 90;// (180f - angleStep) / 2f;
		float oneMinusWallThichness = size * (1 - wallPercentThickness);// / Mathf.Sin (Mathf.Deg2Rad * offsetAngle)));

		switch (type)
		{
			case ShapeMeshType.Basic:
				for (int i = 0; i < sides; i++)
				{
					outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));

					insidePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (oneMinusWallThichness, 0));
				}
				break;
			case ShapeMeshType.Star:
				float indent = getStarIndentPercent (sides) * size;
				float oneMinusWallIndentThichness = indent * (1 - wallPercentThickness);// / Mathf.Sin (Mathf.Deg2Rad * offsetAngle)));

				for (int i = 0; i < sides; i++)
				{
					outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
					outlinePoints.Add (Quaternion.Euler (0, 0, (i + 0.5f) * angleStep - baseRotation) * new Vector2 (indent, 0));

					insidePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (oneMinusWallThichness, 0));
					insidePoints.Add (Quaternion.Euler (0, 0, (i + 0.5f) * angleStep - baseRotation) * new Vector2 (oneMinusWallIndentThichness, 0));
				}
				break;
			default:
				throw new System.Exception ("ShapeGeneration.GenerateShapeMesh: Unknown shape type - " + type);
		}

		Debug.Assert (outlinePoints.Count == insidePoints.Count, "ShapeGeneration.GenerateShapeMesh: Different number of inside and outside points");

		#endregion

		Mesh mesh = new Mesh ();

		#region Generate Mesh

		// Generate verts
		Vector3[] verts = new Vector3[outlinePoints.Count  * 2];

		for (int i = 0; i < outlinePoints.Count; i++)
		{
			verts[(i * 2) + 0] = outlinePoints[i];
			verts[(i * 2) + 1] = insidePoints[i];
		}

		// Generate tris
		int[] tris = new int[outlinePoints.Count * 3 * 2];

		for (int i = 0; i < outlinePoints.Count; i++)
		{
			tris[(i * 3 * 2) + 0] = (i * 2);
			tris[(i * 3 * 2) + 1] = (i * 2) + 1;
			tris[(i * 3 * 2) + 2] = (((i + 1) % outlinePoints.Count) * 2) + 1;

			tris[(i * 3 * 2) + 3] = (i * 2);
			tris[(i * 3 * 2) + 4] = (((i + 1) % outlinePoints.Count) * 2) + 1;
			tris[(i * 3 * 2) + 5] = (((i + 1) % outlinePoints.Count) * 2);
		}

		// Generate uvs
		Vector2[] uvs = new Vector2[verts.Length];

		Vector2 centre = Vector2.zero;
		float left = 0;
		float right = 0;
		float top = 0;
		float bottom = 0;

		for (int i = 1; i < verts.Length; i++)
		{
			centre += (Vector2)verts[i];

			if (verts[i].x > right) right = verts[i].x;
			else if (verts[i].x < left) left = verts[i].x;

			if (verts[i].y > top) top = verts[i].x;
			else if (verts[i].y < bottom) bottom = verts[i].x;
		}

		// Get average centre
		centre /= verts.Length;
		float width = right - left;
		float height = top - bottom;

		for (int i = 0; i < uvs.Length; i++)
		{
			uvs[i] = ((Vector2)verts[i] - centre);
			uvs[i].x /= width;
			uvs[i].y /= height;
		}

		// Generate normals
		Vector3[] normals = new Vector3[verts.Length];
		for (int i = 0; i < normals.Length; i++) normals[i] = Vector3.forward;

		// Generate mesh from data
		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.uv = uvs;
		mesh.normals = normals;

		#endregion

		return mesh;
	}

	public static List<Vector2> GenerateShapeOutline (float size, int sides, float baseRotation, ShapeMeshType type)
	{
		if (size < 0) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeOutline: Size must be greater than 0");
		if (sides < 3) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeOutline: Size must be at least 3");

		List<Vector2> outlinePoints = new List<Vector2> ();

		#region Generate Outline Points

		float angleStep = 360f / sides;

		switch (type)
		{
			case ShapeMeshType.Basic:
				for (int i = 0; i < sides; i++) outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
				break;
			case ShapeMeshType.Star:
				float indent = getStarIndentPercent (sides) * size;

				for (int i = 0; i < sides; i++)
				{
					outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
					outlinePoints.Add (Quaternion.Euler (0, 0, (i + 0.5f) * angleStep - baseRotation) * new Vector2 (indent, 0));
				}
				break;
			default:
				throw new System.Exception ("ShapeGeneration.GenerateShapeOutline: Unknown shape type - " + type);
		}

		#endregion

		return outlinePoints;
	}

	public static List<Vector2> GenerateShapeOutline (float size, int sides, float baseRotation, ShapeMeshType type, float wallPercentThickness)
	{
		if (size < 0) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeOutline: Size must be greater than 0");
		if (sides < 3) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeOutline: Size must be at least 3");
		if (wallPercentThickness < 0 || wallPercentThickness > 1) throw new System.ArgumentException ("ShapeGeneration.GenerateShapeOutline: Wall percent thickness must be between 0 and 1");

		List<Vector2> outlinePoints = new List<Vector2> ();

		#region Generate Outline Points

		float angleStep = 360f / sides;

		switch (type)
		{
			case ShapeMeshType.Basic:
				for (int i = 0; i < sides; i++)
				{
					outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
				}
				break;
			case ShapeMeshType.Star:
				float indent = getStarIndentPercent (sides) * size;

				for (int i = 0; i < sides; i++)
				{
					outlinePoints.Add (Quaternion.Euler (0, 0, i * angleStep - baseRotation) * new Vector2 (size, 0));
					outlinePoints.Add (Quaternion.Euler (0, 0, (i + 0.5f) * angleStep - baseRotation) * new Vector2 (indent, 0));
				}
				break;
			default:
				throw new System.Exception ("ShapeGeneration.GenerateShapeOutline: Unknown shape type - " + type);
		}

		#endregion

		return outlinePoints;
	}

	private static float getStarIndentPercent (int sides)
	{
		return (0.5f - Mathf.Exp (-sides * 0.4f));
	}
}

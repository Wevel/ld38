﻿using System.Collections.Generic;
using UnityEngine;

public static class MapGeneration
{
	public static List<Vector2> GenerateMapOutline (float size, float gridSize, float controlPointScaleFactor, int minOutlinePoints, int seed)
	{
		List<Vector2> outlinePoints = new List<Vector2> ();

		#region Generate Outline Points

		System.Random rnd = new System.Random (seed);

		Vector2 perlinOffset = new Vector2 ((float)rnd.NextDouble () + 3, (float)rnd.NextDouble () + 3);

		int numPoints = getOutlineNumberPoints (size, controlPointScaleFactor, minOutlinePoints);

		float angleStep = 360f / numPoints;

		for (int i = 0; i < numPoints; i++) outlinePoints.Add (getActualPoint (i * angleStep, gridSize, size, perlinOffset));

		// Remove Duplicate points
		for (int i = outlinePoints.Count - 1; i >= 1; i--) if (outlinePoints[i] == outlinePoints[i - 1]) outlinePoints.RemoveAt (i);

		#endregion

		return outlinePoints;
	}

	public static Mesh GenerateMapMesh (List<Vector2> outlinePoints)
	{
		Mesh mesh = new Mesh ();

		#region Generate Mesh

		// Generate verts
		Vector3[] verts = new Vector3[outlinePoints.Count + 1];
		verts[0] = Vector3.zero;

		for (int i = 0; i < outlinePoints.Count; i++) verts[i + 1] = outlinePoints[i];

		// Generate tris
		int[] tris = new int[outlinePoints.Count * 3];

		for (int i = 0; i < outlinePoints.Count; i++)
		{
			tris[(i * 3) + 0] = 0;
			tris[(i * 3) + 1] = i + 1;
			tris[(i * 3) + 2] = ((i + 1) % outlinePoints.Count) + 1;
		}

		// Generate uvs
		Vector2[] uvs = new Vector2[verts.Length];

		Vector2 centre = Vector2.zero;
		float left = 0;
		float right = 0;
		float top = 0;
		float bottom = 0;

		for (int i = 1; i < verts.Length; i++)
		{
			centre += (Vector2)verts[i];

			if (verts[i].x > right) right = verts[i].x;
			else if (verts[i].x < left) left = verts[i].x;

			if (verts[i].y > top) top = verts[i].x;
			else if (verts[i].y < bottom) bottom = verts[i].x;
		}

		// Get average centre
		centre /= verts.Length;
		float width = right - left;
		float height = top - bottom;

		for (int i = 0; i < uvs.Length; i++)
		{
			uvs[i] = ((Vector2)verts[i] - centre);
			uvs[i].x /= width;
			uvs[i].y /= height;
		}

		// Generate normals
		Vector3[] normals = new Vector3[verts.Length];
		for (int i = 0; i < normals.Length; i++) normals[i] = Vector3.forward;
		
		// Generate mesh from data
		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.uv = uvs;
		mesh.normals = normals;

		#endregion

		return mesh;
	}

	private static Vector2 getActualPoint (float angle, float gridSize, float size, Vector2 perlinOffset)
	{
		Vector2 basePoint = Quaternion.Euler (0, 0, angle) * new Vector2 (size, 0);
		Vector2 point = Quaternion.Euler (0, 0, angle) * new Vector2 (size + Mathf.PerlinNoise (basePoint.x / size + perlinOffset.x, basePoint.y / size + perlinOffset.x) * size, 0);
		
		// Make sure each point is on a grid
		point.x = Mathf.Floor (point.x / gridSize) * gridSize;
		point.y = Mathf.Floor (point.y / gridSize) * gridSize;

		return point;
	}

	private static int getOutlineNumberPoints (float size, float controlPointScaleFactor, int minOutlinePoints)
	{
		return Mathf.CeilToInt (size * Mathf.PI * controlPointScaleFactor) + minOutlinePoints;
	}
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ShapeMeshType
{
	/// <summary>
	/// Just an ordinary shape based on the number of sides
	/// </summary>
	Basic,
	/// <summary>
	/// A star shape where the number of sides is the number of points
	/// </summary>
	Star,
}
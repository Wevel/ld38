﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LineGeneration
{
	public static Mesh GenerateLineMesh (List<Vector2> points, float width)
	{
		if (points == null || points.Count <= 1) return null;

		//List<Vector2> points = new List<Vector2> ();

		float halfWidth = width / 2f;

		#region Get Points

		//float sqrWidth = width * width;
		//float sqrDoubleWidth = sqrWidth * 4;

		//Vector2 forwardDirection, backwardDirection;

		//bool lastWasCorner = false;

		//for (int i = 0; i < controlPoints.Count; i++)
		//{
		//	if (i > 0 && i < controlPoints.Count - 1 && isCorner (controlPoints[i - 1], controlPoints[i], controlPoints[i + 1]))
		//	{
		//		backwardDirection = (controlPoints[i - 1] - controlPoints[i]).normalized;
		//		forwardDirection = (controlPoints[i + 1] - controlPoints[i]).normalized;

		//		if (lastWasCorner)
		//		{
		//			if ((controlPoints[i - 1] - controlPoints[i]).sqrMagnitude > sqrDoubleWidth) points.Add (controlPoints[i] + (backwardDirection * halfWidth));
		//			if ((controlPoints[i + 1] - controlPoints[i]).sqrMagnitude > sqrDoubleWidth) points.Add (controlPoints[i] + (forwardDirection * halfWidth));
		//		}
		//		else
		//		{
		//			if ((controlPoints[i - 1] - controlPoints[i]).sqrMagnitude > sqrWidth) points.Add (controlPoints[i] + (backwardDirection * halfWidth));
		//			if ((controlPoints[i + 1] - controlPoints[i]).sqrMagnitude > sqrWidth) points.Add (controlPoints[i] + (forwardDirection * halfWidth));
		//		}

		//		lastWasCorner = true;
		//	}
		//	else
		//	{
		//		points.Add (controlPoints[i]);

		//		lastWasCorner = false;
		//	}
		//}

		for (int i = points.Count - 1; i >= 1; i--)
		{
			if (points[i] == points[i - 1]) points.RemoveAt (i);
		}

		if (points.Count <= 1) return null;

		#endregion
		#region Generate Mesh

		List<Vector3> verts = new List<Vector3> ();
		List<int> tris = new List<int> ();

		float actualHalfWidth;
		float angle;

		for (int i = 0; i < points.Count; i++)
		{
			actualHalfWidth = halfWidth;

			if (i == 0) angle = getAngle (points[0] + points[0] - points[1], points[0], points[1], ref actualHalfWidth);
			else if (i == points.Count - 1) angle = getAngle (points[i - 1], points[i], points[i] + points[i] - points[i - 1], ref actualHalfWidth);
			else angle = getAngle (points[i - 1], points[i], points[i + 1], ref actualHalfWidth);

			// Clamp the width used, so that it doesnt go out of the screen
			actualHalfWidth = Mathf.Clamp (actualHalfWidth, 0, width * 5);

			verts.Add ((Vector3)points[i] + (Quaternion.Euler (0, 0, angle) * new Vector3 (-actualHalfWidth, 0, 0)));
			verts.Add ((Vector3)points[i] + (Quaternion.Euler (0, 0, angle) * new Vector3 (actualHalfWidth, 0, 0)));

			if (i < points.Count - 1)
			{
				tris.Add ((i * 2) + 0);
				tris.Add ((i * 2) + 1);
				tris.Add ((i * 2) + 2);

				tris.Add ((i * 2) + 1);
				tris.Add ((i * 2) + 3);
				tris.Add ((i * 2) + 2);
			}
		}

		Vector2[] uvs = new Vector2[verts.Count];

		float length = 0;

		for (int i = 0; i < points.Count - 1; i++) length += Vector2.Distance (points[i], points[i + 1]);

		float currentLength = 0;

		for (int i = 0; i < points.Count - 1; i++)
		{
			uvs[(2 * i) + 0] = new Vector2 (currentLength / length, 0);
			uvs[(2 * i) + 1] = new Vector2 (currentLength / length, 1);
			currentLength += Vector2.Distance (points[i], points[i + 1]);
		}

		uvs[uvs.Length - 2] = new Vector2 (1, 0);
		uvs[uvs.Length - 1] = new Vector2 (1, 1);

		// Generate normals
		Vector3[] normals = new Vector3[verts.Count];
		for (int i = 0; i < verts.Count; i++) normals[i] = Vector3.forward;

		Mesh mesh = new Mesh ();
		mesh.vertices = verts.ToArray ();
		mesh.triangles = tris.ToArray ();
		mesh.normals = normals;
		mesh.uv = uvs;

		#endregion

		return mesh;
	}

	public static List<Vector2> Outline (List<Vector2> points, float width)
	{
		List<Vector2> outline = new List<Vector2> ();

		float halfWidth = width / 2f;
		float actualHalfWidth, angle;

		for (int i = 0; i < points.Count; i++)
		{
			actualHalfWidth = halfWidth;

			if (i == 0) angle = getAngle (points[0] + points[0] - points[1], points[0], points[1], ref actualHalfWidth);
			else if (i == points.Count - 1) angle = getAngle (points[i - 1], points[i], points[i] + points[i] - points[i - 1], ref actualHalfWidth);
			else angle = getAngle (points[i - 1], points[i], points[i + 1], ref actualHalfWidth);

			// Clamp the width used, so that it doesnt go out of the screen
			actualHalfWidth = Mathf.Clamp (actualHalfWidth, 0, width * 5);

			outline.Add ((Vector3)points[i] + (Quaternion.Euler (0, 0, angle) * new Vector3 (-actualHalfWidth, 0, 0)));
		}

		for (int i = points.Count - 1; i >= 0; i--)
		{
			actualHalfWidth = halfWidth;

			if (i == 0) angle = getAngle (points[0] + points[0] - points[1], points[0], points[1], ref actualHalfWidth);
			else if (i == points.Count - 1) angle = getAngle (points[i - 1], points[i], points[i] + points[i] - points[i - 1], ref actualHalfWidth);
			else angle = getAngle (points[i - 1], points[i], points[i + 1], ref actualHalfWidth);

			// Clamp the width used, so that it doesnt go out of the screen
			actualHalfWidth = Mathf.Clamp (actualHalfWidth, 0, width * 5);

			outline.Add ((Vector3)points[i] + (Quaternion.Euler (0, 0, angle) * new Vector3 (actualHalfWidth, 0, 0)));
		}

		
		return outline;
	}

	public static float GetLength (List<Vector2> points)
	{
		float length = 0;

		for (int i = 0; i < points.Count - 1; i++)
		{
			length += Vector2.Distance (points[i], points[i + 1]);
		}

		return length;
	}

	//public static List<Vector2> GenerateLinePoints (List<Vector2> controlPoints, float width)
	//{
	//	if (controlPoints == null || controlPoints.Count <= 1) return null;

	//	List<Vector2> points = new List<Vector2> ();

	//	#region Get Points

	//	float halfWidth = width / 2f;
	//	float sqrWidth = width * width;
	//	float sqrDoubleWidth = sqrWidth * 4;

	//	Vector2 forwardDirection, backwardDirection;

	//	bool lastWasCorner = false;

	//	for (int i = 0; i < controlPoints.Count; i++)
	//	{
	//		if (i > 0 && i < controlPoints.Count - 1 && isCorner (controlPoints[i - 1], controlPoints[i], controlPoints[i + 1]))
	//		{
	//			backwardDirection = (controlPoints[i - 1] - controlPoints[i]).normalized;
	//			forwardDirection = (controlPoints[i + 1] - controlPoints[i]).normalized;

	//			if (lastWasCorner)
	//			{
	//				if ((controlPoints[i - 1] - controlPoints[i]).sqrMagnitude > sqrDoubleWidth) points.Add (controlPoints[i] + (backwardDirection * halfWidth));
	//				if ((controlPoints[i + 1] - controlPoints[i]).sqrMagnitude > sqrDoubleWidth) points.Add (controlPoints[i] + (forwardDirection * halfWidth));
	//			}
	//			else
	//			{
	//				if ((controlPoints[i - 1] - controlPoints[i]).sqrMagnitude > sqrWidth) points.Add (controlPoints[i] + (backwardDirection * halfWidth));
	//				if ((controlPoints[i + 1] - controlPoints[i]).sqrMagnitude > sqrWidth) points.Add (controlPoints[i] + (forwardDirection * halfWidth));
	//			}

	//			lastWasCorner = true;
	//		}
	//		else
	//		{
	//			points.Add (controlPoints[i]);

	//			lastWasCorner = false;
	//		}
	//	}

	//	for (int i = points.Count - 1; i >= 1; i--)
	//	{
	//		if (points[i] == points[i - 1]) points.RemoveAt (i);
	//	}

	//	if (points.Count <= 1) return null;

	//	#endregion

	//	return points;
	//}

	private static bool isCorner (Vector2 a, Vector2 b, Vector2 c)
	{
		if (c.x == a.x)
		{
			if (b.x == a.x) return false;
			return true;
		}
		if (c.y == a.y)
		{
			if (b.y == a.y) return false;
			return true;
		}

		float gradient = (c.y - a.y) / (c.x - a.x);
		float intercept = a.y - (gradient * a.x);
		float expectedY = (gradient * b.x) + intercept;

		if (expectedY == b.y) return false;
		return true;
	}

	private static float getAngle (Vector2 a, Vector2 b, Vector2 c, ref float halfwidth)
	{
		Vector2 ba = a - b;
		Vector2 bc = c - b;

		float magnitude = ba.magnitude * bc.magnitude;

		float halfFullAngle = magnitude != 0 ? Mathf.Acos (Mathf.Clamp (Vector2.Dot (ba, bc) / magnitude, -1f, 1f)) / 2f : 0;
		float sinAngle = Mathf.Sin (halfFullAngle);

		if (sinAngle != 0) halfwidth /= sinAngle;

		float baseAngle = Mathf.Atan2 (bc.y, bc.x);

		if (c.x != a.x)
		{
			if (c.y != a.y)
			{
				float gradient = (c.y - a.y) / (c.x - a.x);
				float intercept = a.y - (gradient * a.x);

				if (c.x > a.x)
				{
					if ((gradient * b.x) + intercept < b.y) return (baseAngle - halfFullAngle) * Mathf.Rad2Deg;
					else return 180 + ((baseAngle + halfFullAngle) * Mathf.Rad2Deg);
				}
				else
				{
					if ((gradient * b.x) + intercept > b.y) return (baseAngle - halfFullAngle) * Mathf.Rad2Deg;
					else return 180 + ((baseAngle + halfFullAngle) * Mathf.Rad2Deg);
				}
			}
			else
			{
				if (b.y < a.y) return (baseAngle - halfFullAngle) * Mathf.Rad2Deg;
				else return 180 + ((baseAngle + halfFullAngle) * Mathf.Rad2Deg);
			}
		}
		else
		{
			if (b.x > a.x) return (baseAngle - halfFullAngle) * Mathf.Rad2Deg;
			else return 180 + ((baseAngle + halfFullAngle) * Mathf.Rad2Deg);
		}
	}
}

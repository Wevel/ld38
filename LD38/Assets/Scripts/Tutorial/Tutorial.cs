﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	public GameObject tutorialUI;

	public Text tutorialText;
	public Transform tutorialPointer;

	public CameraControl cameraControl;
	public float cameraMoveSpeed = 5;
	public float cameraZoomSpeed = 5;

	public Button removeButton;

	public List<Button> buildButtons;

	GameControl control;
	Map map;

	Coroutine moveCameraCoroutine;

	private void Awake ()
	{
		control = this.GetComponent<GameControl> ();
		map = this.GetComponent<Map> ();

		hidePointer ();
		tutorialUI.SetActive (false);
	}

	public void StartTutorial ()
	{
		tutorialUI.SetActive (true);
		removeButton.interactable = false;
		StartCoroutine (tutorial ());
	}

	private IEnumerator tutorial ()
	{
		yield return null;

		limitBuildable ();

		setText (
@"Welcome to " + control.gameName + @"
A game made in 48 hours for Ludum Dare 38
This tutorial will help teach you how to play

Click to continue");

		while (!hasClicked ()) yield return null;
		yield return null;

		setText (
@"Buildings can be selected using left click or deselected with right click
Use middle mouse to zoom and move the camera

Click to continue");

		while (!hasClicked ()) yield return null;
		yield return null;

		ResourceSpawn resourceArea = findResourceSpawn ("Iron-Ore");

		if (resourceArea != null)
		{
			setPointer (resourceArea.transform.position);
			moveCamera (resourceArea.transform.position, 5);

			setText (
	@"Resource area can be used to mine raw ore from the asteroid
This is Iron-Ore

Click to continue");
		}

		while (!hasClicked ()) yield return null;
		yield return null;

		resourceArea = findResourceSpawn ("Copper-Ore");

		if (resourceArea != null)
		{
			setPointer (resourceArea.transform.position);
			moveCamera (resourceArea.transform.position, 5);

			setText (
	@"Resource area can be used to mine raw ore from the asteroid
This is Copper-Ore

Click to continue");

			while (!hasClicked ()) yield return null;
			yield return null;
		}

		hidePointer ();

		limitBuildable ("Mine");

		setText (
@"Mines Can only be built on a resource area
They produce 1 of the ore every second

Place a mine on Copper-Ore to Continue");

		Building mine;

		while (!isBuilding (
			"Mine", 
			(item) => 
			{
				ResourceSpawn spawn = map.GetResourceArea (item.position);

				return spawn != null && spawn.type.name == "Copper-Ore";
			}, out mine)) yield return null;
		yield return null;

		limitBuildable ("Mine", "Warehouse");

		setText (
@"Warehouses can store up to 25 items at a time
These will be automatically sent to any building that needs them

Place warehouse to Continue");

		Building warehouse;

		while (!isBuilding ("Warehouse", out warehouse)) yield return null;
		yield return null;

		setPointer (mine.position);

		setText (
@"Buildings can be connected together to allow the transfere of items

Select the mine to start");

		while (control.currentBuilding != mine) yield return null;
		yield return null;

		setPointer (warehouse.position);

		setText (
@"Now click 'Create Output' then select the warehouse to complete the connection

Create an output from the mine to continue");

		while (!isConnection (mine, "Warehouse")) yield return null;
		yield return null;

		hidePointer ();

		limitBuildable ("Mine", "Warehouse", "Refiner");

		setText (
@"Items can now move accross to the warehouse
Ores can be turned into plate using a refiner

Build a refiner to continue");

		Building refiner;

		while (!isBuilding ("Refiner", out refiner)) yield return null;
		yield return null;

		setPointer (refiner.position);

		setText (
@"Refiners and crafters need to have a recipe set before they can work
Notice that the building was purple to start with
This mean it needs a recipe

Select the refiner to continue");

		while (control.currentBuilding != refiner) yield return null;
		yield return null;

		hidePointer ();

		setText (
@"Now select a recipe from the drop down menu
If the building turns red, then it hasn't got the required inputs so the warehouse needs to be connected up to it

Set the Smelt-Copper-Ore recipe");

		while (refiner.recipe == null || refiner.recipe.name != "Smelt-Copper-Ore") yield return null;
		yield return null;

		setPointer (new Vector2 (0, 0));

		setText (
@"When a building is green it is working correctly
If a building turns yellow then it can't output its items
Items can be sold at the space port

Connect the refiner to the space port");

		while (!isConnection (refiner, "SpacePort")) yield return null;
		yield return null;

		hidePointer ();

		setText (
@"Now a sell recipe can be selected so that items can be sold to make profit

Sell an item to continue");

		while (!control.bank.hasSoldItem) yield return null;
		yield return null;

		allowAllBuildable ();

		setText (
@"The crafter can make more complecated items to be sold for more credits

Click to continue");

		while (!hasClicked ()) yield return null;
		yield return null;

		setText (
@"Your aim is to make and export as maney satelites as you can
Getting a large amount of money will mean you are well on your way to this goal

Have at least 20K₪ to finish the tutorial");

		while (control.bank.currentMoney < 20000) yield return null;
		yield return null;

		setText (
@"Well done, you are now ready to play the game

Click to finish");

		while (!hasClicked ()) yield return null;
		yield return null;

		tutorialComplete ();
	}

	private bool isConnection (Building building, string targetType)
	{
		foreach (TransportLine item in building.transportInterface.Outputs)
		{
			if (item.end is Building)
			{
				if (((Building)item.end).type.name == targetType) return true;
			}
		}

		return false;
	}

	private void moveCamera (Vector2 position, float zoom)
	{
		if (moveCameraCoroutine != null) StopCoroutine (moveCameraCoroutine);
		moveCameraCoroutine = StartCoroutine (smoothMoveCamera (position, zoom));
	}

	private bool hasClicked ()
	{
		return Camera.main.pixelRect.Contains (Input.mousePosition) && Input.GetMouseButtonUp (0);
	}

	private bool isBuilding (string type, out Building building)
	{
		building = null;

		foreach (Building item in map.PlacedBuildings)
		{
			if (item.type.name == type)
			{
				building = item;
				return true;
			}
		}

		return false;
	}

	private bool isBuilding (string type, System.Predicate<Building> match, out Building building)
	{
		building = null;

		foreach (Building item in map.PlacedBuildings)
		{
			if (item.type.name == type && match (item))
			{
				building = item;
				return true;
			}
		}

		return false;
	}

	private ResourceSpawn findResourceSpawn (string type)
	{
		foreach (ResourceSpawn item in map.ResourceAreas)
		{
			if (item.type.name == type) return item;
		}

		return null;
	}

	private void tutorialComplete ()
	{
		tutorialUI.SetActive (false);

		hidePointer ();
		allowAllBuildable ();
		removeButton.interactable = true;
	}

	IEnumerator smoothMoveCamera (Vector2 point, float zoom)
	{
		cameraControl.canMove = false;

		Vector3 newPosition;

		while (Vector2.Distance (point, Camera.main.transform.position) > cameraMoveSpeed * Time.deltaTime)
		{
			newPosition = Vector2.Lerp (Camera.main.transform.position, point, cameraMoveSpeed * Time.deltaTime);
			newPosition.z = Camera.main.transform.position.z;
			Camera.main.transform.position = newPosition;

			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, zoom, cameraZoomSpeed * Time.deltaTime);

			yield return new WaitForEndOfFrame ();
		}

		cameraControl.canMove = true;

		moveCameraCoroutine = null;
	}

	private void setText (string text)
	{
		tutorialText.text = text;
	}

	private void setPointer (Vector2 point)
	{
		tutorialPointer.gameObject.SetActive (true);
		tutorialPointer.position = point;
	}

	private void hidePointer ()
	{
		tutorialPointer.gameObject.SetActive (false);
	}

	private void allowAllBuildable ()
	{
		for (int i = 0; i < buildButtons.Count; i++) buildButtons[i].interactable = true;
	}

	private void limitBuildable (params string[] types)
	{
		List<string> buildable = new List<string> (types);
		for (int i = 0; i < buildButtons.Count; i++) buildButtons[i].interactable = buildable.Contains (buildButtons[i].transform.GetChild (0).GetComponent<Text> ().text.Trim ());
	}
}

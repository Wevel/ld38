﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
	public int maxUnusedRenderers = 100;
	public float itemZValue = -1;
	public float mineTime = 1f;
	public Material itemMaterial;

	private List< ShapeRenderer> currentRenderers = new List<ShapeRenderer> ();
	private Queue<ShapeRenderer> unusedRenderers = new Queue<ShapeRenderer> ();

	private Dictionary<Building, float> craftTimers = new Dictionary<Building, float> ();

	public void UpdateResourceCollect (Building building, float deltaTime, params string[] producedItems)
	{
		// If the building can't output then it wont run
		if (building.CanCraft)
		{
			if (!craftTimers.ContainsKey (building)) craftTimers[building] = deltaTime;
			else craftTimers[building] += deltaTime;

			if (craftTimers[building] > mineTime)
			{
				craftTimers[building] -= mineTime;

				for (int i = 0; i < producedItems.Length; i++) building.transportInterface.AddToOutputQueue (producedItems[i]);
				if (producedItems.Length > 0) Sound.PlaySound (producedItems[0], SoundType.Mine);
			}
		}
	}

	public void UpdateItemCraft (Building building, float deltaTime)
	{
		if (building.CanCraft)
		{
			if (!craftTimers.ContainsKey (building)) craftTimers[building] = deltaTime;
			else craftTimers[building] += deltaTime;

			if (craftTimers[building] > building.recipe.craftTime)
			{
				craftTimers[building] -= building.recipe.craftTime;

				building.recipe.CompleteOutput (building);
			}
		}
	}

	public ShapeRenderer GetItemDispaly (Item item)
	{
		ShapeRenderer shape = getNextShapeRenderer ();
		//shape.color = Random.ColorHSV ();
		shape.gameObject.SetActive (true);
		currentRenderers.Add (shape);

		item.type.SetupShapeRenderer (shape);
		//item.type.SetupPolygonCollider (shape.GetComponent<PolygonCollider2D> ());

		shape.transform.position = new Vector3 (0, 0, itemZValue);

		return shape;
	}

	public void ReleaseItemDisplay (ShapeRenderer shape)
	{
		if (shape == null) return;
		if (currentRenderers.Contains (shape))
		{
			shape.gameObject.SetActive (false);

			currentRenderers.Remove (shape);

			if (unusedRenderers.Count < maxUnusedRenderers) unusedRenderers.Enqueue (shape);
			else Destroy (shape.gameObject);
		}
	}

	private ShapeRenderer getNextShapeRenderer ()
	{
		if (unusedRenderers.Count > 0) return unusedRenderers.Dequeue ();

		GameObject go = new GameObject ("Item");
		go.transform.SetParent (transform);
		go.layer = DataManager.ItemLayer;

		ShapeRenderer shape = go.AddComponent<ShapeRenderer> ();
		shape.material = itemMaterial;

		//PolygonCollider2D polygonCollider = go.AddComponent<PolygonCollider2D> ();
		//polygonCollider.isTrigger = true;

		//Rigidbody2D shapeRigidbody = go.AddComponent<Rigidbody2D> ();
		//shapeRigidbody.gravityScale = 0;
		//shapeRigidbody.freezeRotation = true;
		//shapeRigidbody.bodyType = RigidbodyType2D.Kinematic;

		return shape;
	}
}

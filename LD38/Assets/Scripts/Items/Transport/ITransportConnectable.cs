﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITransportConnectable
{
	TransportInterface transportInterface
	{
		get;
	}

	bool TryGiveItem (Item item, ItemArrived onArrive, ShapeRenderer display, ITransportConnectable destination);
	bool IsItemNeeded (List<ITransportConnectable> previouseNodes, ItemType item, out ITransportConnectable destination, out uint count);
	bool IsItemNeeded (List<ITransportConnectable> previouseNodes, ItemType item, out ITransportConnectable destination);
}

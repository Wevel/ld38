﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportLine : MonoBehaviour
{
	public ITransportConnectable start { get; private set; }
	public ITransportConnectable end { get; private set; }
	public float transportSpeed { get; private set; }

	public bool blocked { get; private set; }

	public PolygonCollider2D polygonCollider { get; private set; }

	private List<Vector2> points;

	private List<ItemEntity> items = new List<ItemEntity> ();
	private ItemEntity lastAdded = null;

	private float sqrHalthWidth;

	private LineRenderer2D mainLine;
	private LineRenderer2D outline;

	public float length { get; private set; }

	private void Awake ()
	{
		if (polygonCollider == null) polygonCollider = this.GetComponent<PolygonCollider2D> (); ;
	}

	public void DestroyLine (Map map, int direction)
	{
		if (direction <= 0)
		{
			if (start != null)
			{
				if (start is TransportLineMidpoint)
				{
					if (((TransportLineMidpoint)start) != null)
					{
						List<TransportLine> forward = new List<TransportLine> ();
						List<TransportLine> backward = new List<TransportLine> ();

						foreach (TransportLine item in map.TransportLines)
						{
							if (item != this && item != null)
							{
								if (item.end == start) backward.Add (item);
								else if (item.start == start) forward.Add (item);
							}
						}

						foreach (TransportLine item in forward) item.DestroyLine (map, 1);
						foreach (TransportLine item in backward) item.DestroyLine (map, -1);

						start.transportInterface.Clear ();
						Destroy (((TransportLineMidpoint)start).gameObject);
					}
				}
				else if (start is Building)
				{
					start.transportInterface.RemoveOutput (this);
				}
			}
		}

		if (direction >= 0)
		{
			if (end != null)
			{
				if (end is TransportLineMidpoint)
				{
					if (((TransportLineMidpoint)end) != null)
					{
						List<TransportLine> forward = new List<TransportLine> ();
						List<TransportLine> backward = new List<TransportLine> ();

						foreach (TransportLine item in map.TransportLines)
						{
							if (item != this && item != null)
							{
								if (item.end == end) backward.Add (item);
								else if (item.start == end) forward.Add (item);
							}
						}

						foreach (TransportLine item in forward) item.DestroyLine (map, 1);
						foreach (TransportLine item in backward) item.DestroyLine (map, -1);

						start.transportInterface.Clear ();
						Destroy (((TransportLineMidpoint)end).gameObject);
					}
				}
			}
		}

		for (int i = 0; i < items.Count; i++) DataManager.ItemManager.ReleaseItemDisplay (items[i].shape);

		map.RemoveTransportLine (this);

		Destroy (this.gameObject);
	}

	public void Setup (ITransportConnectable start, ITransportConnectable end, List<Vector2> points, float transportSpeed, float width, LineRenderer2D mainLine, LineRenderer2D outline)
	{
		if (polygonCollider == null) polygonCollider = this.GetComponent<PolygonCollider2D> ();

		this.start = start;
		this.end = end;
		this.points = points;
		this.transportSpeed = transportSpeed;
		this.mainLine = mainLine;
		this.outline = outline;

		length = LineGeneration.GetLength (points);

		if (!start.transportInterface.IsOutput(this)) start.transportInterface.AddOutput (this);

		polygonCollider.isTrigger = true;
		polygonCollider.points = LineGeneration.Outline (points, width).ToArray ();

		sqrHalthWidth = width * width / 4f;
	}

	public void Split (TransportLineMidpoint midPoint, Map map)
	{
		int splitIndex = points.IndexOf (midPoint.point);
		if (splitIndex < 0) throw new System.Exception ("TransportLine.Split: Trying to split but doesnt contain split point - " + midPoint.point);
		if (splitIndex == 0 || splitIndex == points.Count - 1) throw new System.Exception ("TransportLine.Split: Trying to split on first or last point");

		GameObject go = new GameObject ("TransportLine(Split)");
		go.transform.SetParent (this.transform.parent);
		go.transform.position = new Vector3 (0, 0, map.transportLineZValue);
		go.transform.rotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;
		go.layer = DataManager.TransportLayer;

		LineRenderer2D line = go.AddComponent<LineRenderer2D> ();
		line.material = map.lineMaterial;
		line.width = map.lineWidth;

		GameObject highlight = new GameObject ("Highlight");
		highlight.transform.SetParent (go.transform);
		highlight.transform.localPosition = new Vector3 (0, 0, 0.1f);
		highlight.transform.rotation = Quaternion.identity;
		highlight.transform.localScale = Vector3.one;

		LineRenderer2D lineHighlight = highlight.AddComponent<LineRenderer2D> ();
		lineHighlight.material = map.lineHighlightMaterial;
		lineHighlight.width = map.lineHighlightWidth;

		/*PolygonCollider2D pc = */go.AddComponent<PolygonCollider2D> ();

		TransportLine transportLine = go.AddComponent<TransportLine> ();

		List<Vector2> firstHalfPoints = points.GetRange (0, splitIndex + 1);
		List<Vector2> secondHalfPoints = points.GetRange (splitIndex, points.Count - splitIndex);

		map.SetupLineRenderer (line, lineHighlight, midPoint, end, transportLine, secondHalfPoints);
		map.SetupLineRenderer (lineHighlight, secondHalfPoints);

		map.SetupLineRenderer (mainLine, outline, start, midPoint, this, firstHalfPoints);
		map.SetupLineRenderer (outline, firstHalfPoints);

		bool doneFirst = false;
		ItemEntity last = null;

		for (int i = 0; i < items.Count; i++)
		{
			if (items[i].currentIndex < splitIndex)
			{
				if (!doneFirst)
				{
					items[i].nextItem = null;
					transportLine.lastAdded = last;
				}

				items[i].end = midPoint;
				items[i].path = items[i].path.GetRange (0, splitIndex + 1);
				doneFirst = true;
			}

			last = items[i];
		}
	}

	public void Transport (Item item, ItemArrived onArrive, ShapeRenderer shape, ITransportConnectable destination)
	{
		if (points == null || points.Count == 1) StartCoroutine (noDistanceTransportItem (item, onArrive, shape, destination));
		else StartCoroutine (transportItem (item, onArrive, shape, destination));
	}

	public bool HasPoint (Vector2 point)
	{
		int index = points.FindIndex (x => Vector2.SqrMagnitude (x - point) < sqrHalthWidth);

		return index >= 0 && index < points.Count - 1;
	}

	public bool IsEndPoint (Vector2 point)
	{
		int index = points.FindIndex (x => Vector2.SqrMagnitude (x - point) < sqrHalthWidth);
		return index <= 0 && index >= points.Count - 1;
	}

	public bool IsRoute (ITransportConnectable building)
	{
		if (end == building) return true;
		if (end.transportInterface.OutputCount == 0) return false;
		foreach (TransportLine item in end.transportInterface.Outputs) if (item.IsRoute (building)) return true;
		return false;
	}

	private IEnumerator transportItem (Item item, ItemArrived onArrive, ShapeRenderer shape, ITransportConnectable destination)
	{
		if (shape == null) shape = DataManager.ItemManager.GetItemDispaly (item);
		Transform itemDisplay = shape.transform;
		float zValue = itemDisplay.position.z;

		ItemEntity entity = new ItemEntity (item, points, lastAdded, end, shape);
		lastAdded = entity;

		items.Add (entity);

		entity.position = entity.path[0];
		itemDisplay.position = new Vector3 (entity.position.x, entity.position.y, zValue);

		yield return new WaitForEndOfFrame ();
		
		Vector3 newPoint;
		float distance;

		// Check if this the last item, and there are more than one item on the line
		// While this item is overlapping the next item in the transport line, mark the line as blocked and wait a frame
		while (entity.TouchingNext)
		{
			shape.enabled = false;
			blocked = true;
			yield return new WaitForEndOfFrame ();
		}

		// When this can move again, mark the transport line as unblocked
		blocked = false;
		shape.enabled = true;
			
		for (int i = 1; i < entity.path.Count; i++)
		{
			entity.currentIndex = i;
			while ((distance = Vector2.Distance(entity.position, entity.path[i])) > Time.deltaTime * transportSpeed)
			{
				if (!MainMenu.Instance.paused && !entity.TouchingNext)
				{
					entity.position = Vector2.Lerp (entity.position, entity.path[i], Time.deltaTime * transportSpeed / distance);
					newPoint = entity.position;
					newPoint.z = zValue;

					itemDisplay.position = newPoint;
				}

				yield return new WaitForEndOfFrame ();
			}

			entity.position = entity.path[i];
			newPoint = entity.position;
			newPoint.z = zValue;

			itemDisplay.position = newPoint;
		}

		// Wait untill it can be moved
		while (!MainMenu.Instance.paused && !entity.end.TryGiveItem (item, onArrive, shape, destination)) yield return new WaitForEndOfFrame ();

		entity.done = true;
		items.Remove (entity);
	}

	private IEnumerator noDistanceTransportItem (Item item, ItemArrived onArrive, ShapeRenderer shape, ITransportConnectable destination)
	{
		while (!end.TryGiveItem (item, onArrive, shape, destination)) yield return new WaitForEndOfFrame ();
	}

	private void OnDrawGizmosSelected ()
	{
		if (points != null)
		{
			Gizmos.color = Color.red;
			for (int i = 0; i < points.Count; i++) Gizmos.DrawSphere (points[i], 0.05f);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportLineMidpoint : MonoBehaviour, ITransportConnectable
{
	public TransportInterface transportInterface { get; private set; }
	public Vector2 point { get; private set; }

	private void Update ()
	{
		if (transportInterface != null && !MainMenu.Instance.paused) transportInterface.UpdateOutputs ();
	}

	public void Setup (Vector2 point, TransportLine toSplit, Map map)
	{
		this.point = point;

		transportInterface = new TransportInterface (this);

		toSplit.Split (this, map);
	}

	public bool TryGiveItem (Item item, ItemArrived onArrive, ShapeRenderer display, ITransportConnectable destination)
	{
		if (transportInterface.OutputQueueSize < 1)
		{
			if (destination != null)
			{
				foreach (TransportLine output in transportInterface.Outputs)
				{
					if (output.IsRoute (destination))
					{
						transportInterface.AddToOutputQueue (item.type, 1, output, onArrive, display);
						return true;
					}
				}
			}

			transportInterface.AddToOutputQueue (item.type, 1, null, onArrive, display);

			return true;
		}
		else
		{
			return false;
		}
	}

	public bool IsItemNeeded (List<ITransportConnectable> previouseNodes, ItemType item, out ITransportConnectable destination)
	{
		destination = null;
		if (previouseNodes.Contains (this)) return false;
		previouseNodes.Add (this);
		foreach (TransportLine output in transportInterface.Outputs) if (output.end.IsItemNeeded (previouseNodes, item, out destination)) return true;		
		return false;
	}

	public bool IsItemNeeded (List<ITransportConnectable> previouseNodes, ItemType item, out ITransportConnectable destination, out uint count)
	{
		count = 0;
		destination = null;
		if (previouseNodes.Contains (this)) return false;
		previouseNodes.Add (this);
		foreach (TransportLine output in transportInterface.Outputs) if (output.end.IsItemNeeded (previouseNodes, item, out destination, out count)) return true;		
		return false;
	}
}

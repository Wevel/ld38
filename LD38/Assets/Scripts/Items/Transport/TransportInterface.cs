﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportInterface
{
	public ITransportConnectable connectable { get; private set; }

	private Queue<OutputQueueItem> outputQueue = new Queue<OutputQueueItem> ();
	private List<TransportLine> outputs = new List<TransportLine> ();
	private List<ItemType> storage = new List<ItemType> ();

	private int lastUsedOutput = -1;

	public IEnumerable<TransportLine> Outputs
	{
		get
		{
			for (int i = 0; i < outputs.Count; i++) yield return outputs[i]; 
		}
	}

	public IEnumerable<ItemType> StoredTypes
	{
		get
		{
			List<ItemType> done = new List<ItemType> ();

			for (int i = 0; i < storage.Count; i++)
			{
				if (!done.Contains (storage[i]))
				{
					done.Add (storage[i]);
					yield return storage[i];
				}
			}
		}
	}

	public int OutputCount
	{
		get
		{
			return outputs.Count;
		}
	}

	public int OutputQueueSize
	{
		get
		{
			return outputQueue.Count;
		}
	}

	public int TotalStoredCount
	{
		get
		{
			return storage.Count;
		}
	}

	public int StoredTypesCount
	{
		get
		{
			int count = 0;

			List<ItemType> done = new List<ItemType> ();

			for (int i = 0; i < storage.Count; i++)
			{
				if (!done.Contains (storage[i]))
				{
					done.Add (storage[i]);
					count++;
				}
			}

			return count;
		}
	}

	public bool CanOutput
	{
		get
		{
			// The building can prodice if one of the outputs is not blocked
			return outputs.Find (x => !x.blocked) != null;
		}
	}

	public TransportInterface (ITransportConnectable connectable)
	{
		this.connectable = connectable;
	}

	public void UpdateOutputs ()
	{
		// Make sure that there are outputs
		if (outputs.Count > 0)
		{
			// Check if there is anything to output
			if (outputQueue.Count > 0)
			{
				if (CanOutput)
				{
					// Increment the last output index
					lastUsedOutput++;
					lastUsedOutput = lastUsedOutput % outputs.Count;

					OutputQueueItem type = outputQueue.Dequeue ();

					if (type.specificOutput == null)
					{
						int actualIndex;

						for (int i = 0; i < outputs.Count; i++)
						{
							actualIndex = (i + lastUsedOutput) % outputs.Count;

							if (!outputs[actualIndex].blocked)
							{
								outputs[actualIndex].Transport (new Item (type.item), type.onArrive, type.shape, type.destination);
								break;
							}
						}
					}
					else
					{
						type.specificOutput.Transport (new Item (type.item), type.onArrive, type.shape, type.destination);
					}
				}
			}
		}
	}

	public void Clear ()
	{
		OutputQueueItem item;

		while (outputQueue.Count > 0)
		{
			item = outputQueue.Dequeue ();
			DataManager.ItemManager.ReleaseItemDisplay (item.shape);
		}

		storage.Clear ();
	}

	public void AddOutput (TransportLine transportLine)
	{
		if (transportLine == null) throw new System.ArgumentNullException ("TranportConnection.AddOutput: Transport line is null");

		Debug.Assert (transportLine.start == connectable, "TranportConnection.AddOutput: Transport line start is not this building");

		if (outputs.Contains (transportLine) || outputs.Find (x => x.end == transportLine.end))
			throw new System.Exception ("TranportConnection.AddOutput: Already got a transport line to " + transportLine.end);

		outputs.Add (transportLine);
	}

	public void RemoveOutput (TransportLine transportLine)
	{
		if (outputs.Contains (transportLine)) outputs.Remove (transportLine);
	}

	public void AddToOutputQueue (string typeName, uint count = 1, TransportLine target = null, ItemArrived onArrive = null, ShapeRenderer shape = null, ITransportConnectable destination = null)
	{
		ItemType type = DataManager.GetItemType (typeName);
		if (type == null) throw new System.Exception ("TranportConnection.ProcdueItem: Unknown item type - " + typeName);

		for (int i = 0; i < count; i++) outputQueue.Enqueue (new OutputQueueItem (type, target, onArrive, shape, destination));
	}

	public void AddToOutputQueue (ItemType type, uint count = 1, TransportLine target = null, ItemArrived onArrive = null, ShapeRenderer shape = null, ITransportConnectable destination = null)
	{
		for (int i = 0; i < count; i++) outputQueue.Enqueue (new OutputQueueItem (type, target, onArrive, shape, destination));
	}

	public void OutputFromStorage (string type, uint count, TransportLine target = null, ItemArrived onArrive = null, ShapeRenderer shape = null, ITransportConnectable destination = null)
	{
		if (!outputs.Contains (target)) throw new System.Exception ("TransportInterface.OutputFromStorage: Target is an output");

		for (int i = storage.Count - 1; i >= 0; i--)
		{
			if (storage[i].name == type)
			{
				outputQueue.Enqueue (new OutputQueueItem (storage[i], target, onArrive, shape, destination));
				storage.RemoveAt (i);
				count--;
				if (count == 0) return;
			}
		}
	}

	public void CycleItems ()
	{
		// Get a copy of the stored items
		List<ItemType> oldStoredItems = new List<ItemType> (storage);

		// Clear the old items list
		storage = new List<ItemType> ();

		// Foreach item try to give it back to the building, if this fails then add the item to the output queue
		// This means that any items that are still needed by the new recipe are kept, but any other items are outputted
		for (int i = 0; i < oldStoredItems.Count; i++) if (!connectable.TryGiveItem (new Item (oldStoredItems[i]), null, null, connectable)) AddToOutputQueue (oldStoredItems[i]);
	}

	public void AddToStorage (ItemType type)
	{
		storage.Add (type);
	}

	public bool IsOutput (TransportLine output)
	{
		return outputs.Contains (output);
	}

	public bool TryUseStored (string name, uint count)
	{
		if (!HasStored (name, count)) return false;

		for (int i = storage.Count - 1; i >= 0; i--)
		{
			if (storage[i].name == name)
			{
				count--;
				storage.RemoveAt (i);
				if (count == 0) return true;
			}
		}

		return false;
	}

	public bool HasStored (string name, uint count = 1)
	{
		for (int i = 0; i < storage.Count; i++)
		{
			if (storage[i].name == name)
			{
				count--;
				if (count == 0) return true;
			}
		}

		return false;
	}

	public uint StoredCount (string name)
	{
		uint total = 0;

		for (int i = 0; i < storage.Count; i++) if (storage[i].name == name) total++;

		return total;
	}
}

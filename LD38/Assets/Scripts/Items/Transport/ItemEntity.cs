﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEntity
{
	public Item item;
	public Vector2 position;
	public List<Vector2> path;
	public ItemEntity nextItem;
	public ITransportConnectable end;
	public ShapeRenderer shape;

	public int currentIndex = 0;

	public bool done = false;

	public bool TouchingNext
	{
		get
		{
			if (nextItem != null && nextItem.done)
			{
				nextItem = null;
				return false;
			}

			if (nextItem == null) return false;
			return Vector2.Distance (position, nextItem.position) < item.type.size + nextItem.item.type.size;
		}
	}

	public ItemEntity (Item item, List<Vector2> path, ItemEntity nextItem, ITransportConnectable end, ShapeRenderer shape)
	{
		this.item = item;
		this.path = new List<Vector2> (path);
		this.nextItem = nextItem;
		this.end = end;
		this.shape = shape;
	}
}

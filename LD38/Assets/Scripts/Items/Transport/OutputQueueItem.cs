﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutputQueueItem
{
	public ItemType item { get; private set; }
	public TransportLine specificOutput { get; private set; }

	public ItemArrived onArrive { get; private set; }

	public ShapeRenderer shape { get; private set; }
	public ITransportConnectable destination { get; private set; }

	public OutputQueueItem (ItemType item, TransportLine specificOutput, ItemArrived onArrive, ShapeRenderer shape, ITransportConnectable destination)
	{
		this.item = item;
		this.specificOutput = specificOutput;

		this.onArrive = onArrive;

		this.shape = shape;
		this.destination = destination;
	}
}

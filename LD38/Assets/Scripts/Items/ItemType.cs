﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemType
{
	public string name { get; private set; }
	public float size { get; private set; }
	public int sides { get; private set; }
	public ShapeMeshType type { get; private set; }
	public uint basePrice { get; private set; }
	public Color32 colour { get; private set; }
	public bool isOre { get; private set; }

	//private Vector2[] outlinePoints = null;

	public ItemType (string name, float size, int sides, ShapeMeshType type, uint basePrice, Color32 colour, bool isOre)
	{
		this.name = name;
		this.size = size;
		this.sides = sides;
		this.type = type;
		this.basePrice = basePrice;
		this.colour = colour;
		this.isOre = isOre;
	}

	public void SetupShapeRenderer (ShapeRenderer shape)
	{
		shape.size = size;
		shape.sides = sides;
		shape.type = type;
		shape.outline = false;
		shape.colour = colour;
		shape.Regenerate ();
	}

	//public void SetupPolygonCollider (PolygonCollider2D polygonCollider)
	//{
	//	if (outlinePoints == null) outlinePoints = ShapeGeneration.GenerateShapeOutline (size, sides, 90f - (180f / sides), type).ToArray ();
	//	polygonCollider.points = outlinePoints;
	//}
}

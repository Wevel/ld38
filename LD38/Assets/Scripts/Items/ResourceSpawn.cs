﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceSpawn : MonoBehaviour
{
	public ItemType type { get; private set; }

	private SpriteRenderer sr;

	public void Setup (string itemType, Material material, Sprite icon, Vector2 size)
	{
		type = DataManager.GetItemType (itemType);
		if (type == null) throw new System.Exception ("ResourceSpawn.Setup: Unknown item type - " + itemType);

		sr = this.GetComponent<SpriteRenderer> ();
		if (sr == null) sr = gameObject.AddComponent<SpriteRenderer> ();
		sr.sprite = icon;
		sr.material = material;
		sr.drawMode = SpriteDrawMode.Tiled;
		sr.tileMode = SpriteTileMode.Continuous;
		sr.size = new Vector2 (size.x * icon.rect.width / icon.pixelsPerUnit, size.y * icon.rect.height / icon.pixelsPerUnit);

		sr.color = type.colour;
	}

	public bool Contains (Vector2 point)
	{
		return sr.bounds.Contains (new Vector3 (point.x, point.y, transform.position.z));
	}

	private void OnDrawGizmosSelected ()
	{
		if (sr != null)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (new Vector3 (sr.bounds.min.x, sr.bounds.min.y, 0), new Vector3 (sr.bounds.min.x, sr.bounds.max.y, 0));
			Gizmos.DrawLine (new Vector3 (sr.bounds.min.x, sr.bounds.max.y, 0), new Vector3 (sr.bounds.max.x, sr.bounds.max.y, 0));
			Gizmos.DrawLine (new Vector3 (sr.bounds.max.x, sr.bounds.max.y, 0), new Vector3 (sr.bounds.max.x, sr.bounds.min.y, 0));
			Gizmos.DrawLine (new Vector3 (sr.bounds.max.x, sr.bounds.min.y, 0), new Vector3 (sr.bounds.min.x, sr.bounds.min.y, 0));
		}
	}
}

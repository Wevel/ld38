﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recipe
{
	public string name { get; private set; }
	public float craftTime { get; private set; }

	public int totalInputs { get; private set; }
	public int totalOutputs { get; private set; }

	public bool hasStaredOutput { get; private set; }

	private Dictionary<string, uint> groupedInputs;
	private Dictionary<string, uint> groupedOutputs;

	public IEnumerable<string> Inputs
	{
		get
		{
			foreach (KeyValuePair<string, uint> item in groupedInputs) for (uint i = 0; i < item.Value; i++) yield return item.Key;
		}
	}

	public IEnumerable<string> Outputs
	{
		get
		{
			foreach (KeyValuePair<string, uint> item in groupedOutputs) for (uint i = 0; i < item.Value; i++) yield return item.Key;
		}
	}

	public Recipe (string name, float craftTime, string[] inputs, string[] outputs)
	{
		this.name = name;
		this.craftTime = craftTime;

		this.groupedInputs = new Dictionary<string, uint> ();
		this.groupedOutputs = new Dictionary<string, uint> ();

		ItemType type;

		for (int i = 0; i < inputs.Length; i++)
		{
			if (groupedInputs.ContainsKey (inputs[i])) groupedInputs[inputs[i]]++;
			else groupedInputs.Add (inputs[i], 1);

			type = DataManager.GetItemType (inputs[i]);
			if (type == null) throw new System.Exception ("Recipe(): Unknown item type in input - " + inputs[i]);
		}

		for (int i = 0; i < outputs.Length; i++)
		{
			if (groupedOutputs.ContainsKey (outputs[i])) groupedOutputs[outputs[i]]++;
			else groupedOutputs.Add (outputs[i], 1);

			type = DataManager.GetItemType (outputs[i]);
			if (type == null) throw new System.Exception ("Recipe(): Unknown item type in output - " + outputs[i]);

			if (type.type == ShapeMeshType.Star) hasStaredOutput = true;
		}

		totalInputs = inputs.Length;
		totalOutputs = outputs.Length;
	}

	public bool IsRequired (string name, out uint count)
	{
		if (groupedInputs.ContainsKey (name))
		{
			count = groupedInputs[name];
			return true;
		}

		count = 0;
		return false;
	}

	public bool IsRequired (Building building, string name, out uint count)
	{
		if (groupedInputs.ContainsKey (name))
		{
			count = groupedInputs[name];
			uint alreadyStored = building.transportInterface.StoredCount (name);

			if (count > alreadyStored)
			{
				count -= alreadyStored;
				return true;
			}
		}

		count = 0;
		return false;
	}

	public bool IsRequired (string name)
	{
		return groupedInputs.ContainsKey (name);
	}

	public bool IsRequired (Building building, string name)
	{
		return groupedInputs.ContainsKey (name) && groupedInputs[name] > building.transportInterface.StoredCount (name);
	}

	public bool HasInputs (Building building)
	{
		foreach (KeyValuePair<string, uint> item in groupedInputs) if (!building.transportInterface.HasStored (item.Key, item.Value)) return false;
		return true;
	}

	public void CompleteOutput (Building building)
	{
		if (HasInputs (building))
		{
			foreach (KeyValuePair<string, uint> item in groupedInputs) if (!building.transportInterface.TryUseStored (item.Key, item.Value)) throw new System.Exception ("Recipe.CompleteOutput: The building had the correct inputs but failed to use them");
			foreach (KeyValuePair<string, uint> item in groupedOutputs) building.transportInterface.AddToOutputQueue (item.Key, item.Value);

			// If there is an on finish craft delegate, then run it
			if (building.type.onFinishCraft != null) building.type.onFinishCraft (building, this);

			Sound.PlaySound (building.recipe.name, SoundType.Craft);
		}
	}
}

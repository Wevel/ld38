﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bank : MonoBehaviour
{
	public static uint GetTotalSellValue (Map map, TransportLine line)
	{
		uint total = 0;

		List<TransportLine> closed = new List<TransportLine> ();
		Queue<TransportLine> open = new Queue<TransportLine> ();
		open.Enqueue (line);
		closed.Add (line);
		total += GetTranportLineSellValue (line);

		TransportLine current;

		while (open.Count > 0)
		{
			current = open.Dequeue ();

			foreach (TransportLine item in map.TransportLines)
			{
				if ((current.end is TransportLineMidpoint && item.end == current.end || item.end == current.start) || (current.start is TransportLineMidpoint && (item.start == current.start || item.start == current.end)))
				{
					if (item != current && item != null && !closed.Contains (item))
					{
						open.Enqueue (item);
						closed.Add (item);
						total += GetTranportLineSellValue (item);
					}
				}
			}
		}

		return total;
	}

	public static uint GetTotalSellValue (Map map, Building building)
	{
		uint total = 0;

		foreach (TransportLine item in map.TransportLines)
		{
			if (item.start == (ITransportConnectable)building || item.end == (ITransportConnectable)building) total += GetTotalSellValue (map, item);
		}

		return total;
	}

	public static uint GetTranportLineSellValue (TransportLine line)
	{
		return (uint)Mathf.FloorToInt (Mathf.FloorToInt (line.length * 10) * 0.75f);
	}

	public static uint GetNewTransportLineCost (List<Vector2> points)
	{
		return (uint)Mathf.FloorToInt (LineGeneration.GetLength (points) * 10);
	}

	public ulong startingMoney = 10000;
	public char currencySymbol;

	public ulong currentMoney { get; private set; }

	public bool hasSoldItem { get; private set; }

	public string CurrentMoneyString
	{
		get
		{
			return currentMoney + "" + currencySymbol;
		}
	}

	private void Awake ()
	{
		currentMoney = startingMoney;
		hasSoldItem = false;
	}

	public bool TryBuy (Building building, string itemType, uint count = 1, TransportLine target = null, ItemArrived onArrive = null, ITransportConnectable destination = null)
	{
		ItemType type = DataManager.GetItemType (itemType);
		if (type == null) throw new System.Exception ("Bank.TryBuy: Unknown tile type - " + itemType);

		return TryBuy (building, type, count, target, onArrive, destination);
	}

	public bool TryBuy (Building building, ItemType type, uint count = 1, TransportLine target = null, ItemArrived onArrive = null, ITransportConnectable destination = null)
	{
		uint actualCount = 0;

		for (uint i = 0; i < count; i++) if (TrySpend (getActualValue (type))) actualCount++;

		if (actualCount > 0)
		{
			building.transportInterface.AddToOutputQueue (type, actualCount, target, onArrive, null, destination);
			Sound.PlaySound (type.name, SoundType.Buy);
			return true;
		}

		return false;
	}

	public bool TrySpend (uint amount)
	{
		if (currentMoney >= amount)
		{
			currentMoney -= amount;
			return true;
		}

		return false;
	}

	public bool CanSpend (uint amount)
	{
		return currentMoney >= amount;
	}

	public void SellTransportLine (TransportLine line)
	{
		currentMoney += (uint)Mathf.FloorToInt (Mathf.FloorToInt (line.length * 10) * 0.75f);
	}

	public void SellBuilding (BuildingType type)
	{
		currentMoney += (uint) Mathf.FloorToInt(type.cost * 0.75f);
	}

	public void FinishSellCraft (Building building, Recipe recipe)
	{
		ItemType type;

		foreach (string item in recipe.Inputs)
		{
			type = DataManager.GetItemType (item);
			if (type == null) throw new System.Exception ("Bank.FinishSellCraft: Unknown item type - " + item);
			currentMoney += getActualValue (type);
		}

		Sound.PlaySound (recipe.name, SoundType.Sell);
		hasSoldItem = true;
	}

	private uint getActualValue (ItemType type)
	{
		return type.basePrice; // This would have to be modified by the current demand and the quality/colour of the item
	}
}

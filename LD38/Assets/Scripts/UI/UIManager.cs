﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public GameControl gameControl;

	public GameObject selectedBuildingUI;
	public Dropdown recipeDropdown;
	public Button destroyButton;
	public Button addOutputButton;

	public RecipeDisplay currentRecipeDisplay;
	public StorageDisplay currentStorageDisplay;

	public Text currentCreditsText;
	public Text creditModiferText;

	public Text currentSelectedNameText;

	public Color gainColour;
	public Color lossColour;

	public GameObject itemCostMenu;
	public UIItemCost itemCostLinePrefab;

	private Building lastCurrentBuilding;

	private bool mousedOverDestroy = false;

	private void Start ()
	{
		UIItemCost type;

		foreach (ItemType item in DataManager.PossibleItems)
		{
			type = Instantiate (itemCostLinePrefab, itemCostMenu.transform);
			item.SetupShapeRenderer (type.shape.shape);
			type.costText.text = item.basePrice + "" + gameControl.bank.currencySymbol;
		}
	}

	private void Update ()
	{
		currentCreditsText.text = gameControl.bank.CurrentMoneyString;

		if (gameControl.currentBuildingType != null)
		{
			selectedBuildingUI.SetActive (false);
			creditModiferText.gameObject.SetActive (true);
			itemCostMenu.SetActive (false);

			creditModiferText.color = lossColour;
			creditModiferText.text = "-" + gameControl.currentBuildingType.cost + gameControl.bank.currencySymbol;
		}
		else if (gameControl.isCreatingOutputLine)
		{
			selectedBuildingUI.SetActive (false);
			creditModiferText.gameObject.SetActive (true);
			itemCostMenu.SetActive (false);

			creditModiferText.color = lossColour;
			creditModiferText.text = "-" + gameControl.currentTransportLineCost + gameControl.bank.currencySymbol;
		}
		else if (gameControl.currentBuilding != null)
		{
			currentSelectedNameText.text = gameControl.currentBuilding.type.name;

			addOutputButton.gameObject.SetActive (true);
			selectedBuildingUI.SetActive (true);

			itemCostMenu.SetActive (gameControl.currentBuilding.type.name == "SpacePort");

			destroyButton.gameObject.SetActive (gameControl.currentBuilding.type.name != "SpacePort");

			currentRecipeDisplay.gameObject.SetActive (gameControl.currentBuilding.recipe != null && gameControl.currentBuilding.type.name != "SpacePort");
			if (currentRecipeDisplay.gameObject.activeSelf) currentRecipeDisplay.SetRecipe (gameControl.currentBuilding.recipe);

			currentStorageDisplay.gameObject.SetActive (gameControl.currentBuilding.type.internalStorageSize > 0);
			if (currentStorageDisplay.gameObject.activeSelf) currentStorageDisplay.SetCurrentStorage (gameControl.currentBuilding);

			if (gameControl.currentBuilding.type.needsRecipe)
			{
				recipeDropdown.gameObject.SetActive (true);

				if (lastCurrentBuilding != gameControl.currentBuilding)
				{
					List<string> possibleRecipies = DataManager.GetPossibleRecipies (gameControl.currentBuilding.type);
					possibleRecipies.Insert (0, "");

					recipeDropdown.ClearOptions ();

					List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ();

					for (int i = 0; i < possibleRecipies.Count; i++)
					{
						options.Add (new Dropdown.OptionData (possibleRecipies[i]));
					}

					recipeDropdown.AddOptions (options);

					if (gameControl.currentBuilding.recipe != null)
					{
						int actualIndex = possibleRecipies.FindIndex (x => x == gameControl.currentBuilding.recipe.name);

						if (actualIndex < 0) recipeDropdown.value = 0;
						else recipeDropdown.value = actualIndex;
					}
					else recipeDropdown.value = 0;
				}
			}
			else
			{
				recipeDropdown.gameObject.SetActive (false);
			}

			if (mousedOverDestroy && gameControl.currentBuilding.type.name != "SpacePort")
			{
				creditModiferText.gameObject.SetActive (true);

				creditModiferText.color = gainColour;
				creditModiferText.text = ((uint)Mathf.FloorToInt (gameControl.currentBuilding.type.cost * 0.75f) + Bank.GetTotalSellValue (gameControl.map, gameControl.currentBuilding)) + "" + gameControl.bank.currencySymbol;
			}
			else
			{
				creditModiferText.gameObject.SetActive (false);
			}
		}
		else if (gameControl.currentTransportLine != null)
		{
			selectedBuildingUI.SetActive (true);
			destroyButton.gameObject.SetActive (true);
			recipeDropdown.gameObject.SetActive (false);
			addOutputButton.gameObject.SetActive (false);
			currentRecipeDisplay.gameObject.SetActive (false);
			currentStorageDisplay.gameObject.SetActive (false);
			itemCostMenu.SetActive (false);

			currentSelectedNameText.text = "Transport Line";

			creditModiferText.gameObject.SetActive (false);

			if (mousedOverDestroy)
			{
				creditModiferText.gameObject.SetActive (true);

				creditModiferText.color = gainColour;
				creditModiferText.text = Bank.GetTotalSellValue (gameControl.map, gameControl.currentTransportLine) + "" + gameControl.bank.currencySymbol;
			}
			else
			{
				creditModiferText.gameObject.SetActive (false);
			}
		}
		else
		{
			creditModiferText.gameObject.SetActive (false);
			selectedBuildingUI.SetActive (false);
			itemCostMenu.SetActive (false);
		}

		lastCurrentBuilding = gameControl.currentBuilding;
	}

	public void MouseOverDestroyButton ()
	{
		mousedOverDestroy = true;
	}

	public void MouseOffDestroyButton ()
	{
		mousedOverDestroy = false;
	}

	public void DropDownOutputChanged ()
	{
		if (gameControl.currentBuilding != null)
		{
			if (recipeDropdown.value < 0) gameControl.currentBuilding.SetRecipe ("");
			else gameControl.currentBuilding.SetRecipe (recipeDropdown.options[recipeDropdown.value].text);
		}
	}
}

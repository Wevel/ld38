﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipeDisplay : MonoBehaviour {

	public RectTransform inputHolder;
	public RectTransform outputHolder;

	public UIShapeRenderer itemPrefab;
	public GameObject plusPrefab;

	public ContentSizeFitter sizeFitter;

	private Recipe lastRecipe;

	public void SetRecipe (Recipe recipe)
	{
		if (lastRecipe != recipe)
		{
			lastRecipe = recipe;

			foreach (Transform child in inputHolder) Destroy (child.gameObject);
			foreach (Transform child in outputHolder) Destroy (child.gameObject);

			if (recipe != null)
			{
				bool doneFirst = false;

				UIShapeRenderer shape;
				ItemType type;

				if (recipe.totalInputs > 0)
				{
					foreach (string item in recipe.Inputs)
					{
						type = DataManager.GetItemType (item);

						if (type != null)
						{
							if (doneFirst) Instantiate (plusPrefab).transform.SetParent (inputHolder);
							doneFirst = true;

							shape = Instantiate (itemPrefab);
							shape.transform.SetParent (inputHolder);
							type.SetupShapeRenderer (shape.shape);
						}
					}
				}

				doneFirst = false;

				if (recipe.totalOutputs > 0)
				{
					foreach (string item in recipe.Outputs)
					{
						type = DataManager.GetItemType (item);

						if (type != null)
						{
							if (doneFirst) Instantiate (plusPrefab).transform.SetParent (outputHolder);
							doneFirst = true;

							shape = Instantiate (itemPrefab);
							shape.transform.SetParent (outputHolder);
							type.SetupShapeRenderer (shape.shape);
						}
					}
				}
			}

			LayoutRebuilder.ForceRebuildLayoutImmediate (GetComponent<RectTransform> ());
		}
	}
}

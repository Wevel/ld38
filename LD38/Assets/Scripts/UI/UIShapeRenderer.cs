﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShapeRenderer : MonoBehaviour
{
	public ShapeRenderer shape;
	public Camera cam;
	public RawImage image;

	public int renderTexSize = 256;

	private RenderTexture rt;

	private void Awake ()
	{
		rt = new RenderTexture (renderTexSize, renderTexSize, 24, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
		cam.targetTexture = rt;
		image.texture = rt;
	}
}

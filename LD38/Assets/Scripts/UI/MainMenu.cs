﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public static MainMenu Instance { get; private set; }

	public bool playTutorial { get; private set; }

	public bool playing { get; private set; }
	public bool paused { get; private set; }

	public string gameSceneName;

	public GameObject mainMenuUI;
	public GameObject startGameButton;
	public GameObject restartGameButton;
	public GameObject startTutorialButton;
	public GameObject restartTutorialButton;
	public GameObject resumeButton;

	private void Awake ()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
	}

	private void Update ()
	{
		mainMenuUI.SetActive (!playing || paused);
		
		startGameButton.SetActive (!playing || playTutorial);
		restartGameButton.SetActive (playing && !playTutorial);

		startTutorialButton.SetActive (!playing || !playTutorial);
		restartTutorialButton.SetActive (playing && playTutorial);

		resumeButton.SetActive (playing);

		if (Input.GetKeyDown (KeyCode.Escape)) paused = !paused;
	}

	public void PlayGame (bool playTutorial)
	{
		this.playTutorial = playTutorial;
		playing = true;
		paused = false;

		SceneManager.LoadScene (gameSceneName, LoadSceneMode.Single);
	}

	public void Resume ()
	{
		paused = false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageDisplay : MonoBehaviour
{
	public UIItemCost itemAmountPrefab;

	private List<UIItemCost> types = new List<UIItemCost> ();

	public void SetCurrentStorage (Building building)
	{
		if (building == null)
		{
			foreach (Transform child in transform) Destroy (child.gameObject);
		}
		else
		{
			uint count;

			int i = 0;

			foreach (ItemType item in building.transportInterface.StoredTypes)
			{
				count = building.transportInterface.StoredCount (item.name);

				if (i >= types.Count) types.Add (Instantiate (itemAmountPrefab, transform));

				item.SetupShapeRenderer (types[i].shape.shape);
				types[i].costText.text = count + "";

				i++;
			}

			for (int a = i; a < types.Count; a++) Destroy (types[a]);
		}

		LayoutRebuilder.ForceRebuildLayoutImmediate (GetComponent<RectTransform> ());
	}
}

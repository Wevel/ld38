﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent (typeof (MeshFilter), typeof (MeshRenderer))]
public class Map : MonoBehaviour
{
	public float size = 10;
	public float gridSize = 0.1f;
	public float controlPointScaleFactor = 1;
	public float lineWidth = 0.5f;
	public float lineHighlightWidth = 0.7f;
	public float transportSpeed = 5;
	public int minOutlinePoints = 10;
	public int seed = -1;

	public float transportLineZValue = 1;
	public float buildingZValue = -1;
	public float resourceZValie = 2;

	public float resourceSpread = 2f;

	public Sprite resourceIconSprite;
	public Material resourceDisplayMaterial;

	public Material buildingMaterial;

	public Color needRecipeBuildingColour;
	public Color needInputBuildingColour;
	public Color cantOutputBuildingColour;
	public Color normalRunningBuildingColour;

	public Material lineMaterial;
	public Material lineHighlightMaterial;

	public Collider2D currentBuildingTypeCollider;
	public PolygonCollider2D currentBuildingOutputlineCollider;

	private List<ResourceSpawn> resourceAreas = new List<ResourceSpawn> ();
	private List<Building> placedBuildings = new List<Building> ();
	private List<TransportLine> placedTransportLines = new List<TransportLine> ();
	private MapBounds mapBounds;

	private MeshFilter mf;
	//private MeshRenderer mr;
	private Bank bank;

	ContactFilter2D transportCollisionFilter = new ContactFilter2D ()
	{
		useTriggers = true,
		useLayerMask = true,
		layerMask = 1 << DataManager.TransportLayer,
	};

	ContactFilter2D collisionFilter = new ContactFilter2D ()
	{
		useTriggers = true,
		useLayerMask = true,
		layerMask = 1 << DataManager.BuildingLayer,
	};

	ContactFilter2D buildCollisionFilter = new ContactFilter2D ()
	{
		useTriggers = true,
		useLayerMask = true,
		layerMask = (1 << DataManager.BuildingLayer) | (1 << DataManager.TransportLayer),
	};

	public IEnumerable<Building> PlacedBuildings
	{
		get 
		{
			for (int i = 0; i < placedBuildings.Count; i++) yield return placedBuildings[i];
		}
	}

	public IEnumerable<ResourceSpawn> ResourceAreas
	{
		get
		{
			for (int i = 0; i < resourceAreas.Count; i++) yield return resourceAreas[i];
		}
	}

	public IEnumerable<TransportLine> TransportLines
	{
		get
		{
			for (int i = 0; i < placedTransportLines.Count; i++) yield return placedTransportLines[i];
		}
	}

	private void Awake ()
	{
		mf = this.GetComponent<MeshFilter> ();
		bank = this.GetComponent<Bank> ();
		//mr = this.GetComponent<MeshRenderer> ();

		Debug.Assert (mf != null, "Map.Awake: There needs to be a mesh filter on this gameobject", this);
		//Debug.Assert (mr != null, "Map.Awake: There needs to be a mesh renderer on this gameobject", this);
	}

	private void Update ()
	{
		if (!MainMenu.Instance.paused)
		{
			for (int i = 0; i < placedBuildings.Count; i++) placedBuildings[i].type.onUpdate (placedBuildings[i], Time.deltaTime);
		}
	}

	public void Generate ()
	{
		placedBuildings.Clear ();

		if (mf == null) mf = this.GetComponent<MeshFilter> ();
		//if (mf == null) mr = this.GetComponent<MeshRenderer> ();
		Debug.Assert (mf != null, "Map.Generate: There needs to be a mesh filter on this gameobject", this);
		//Debug.Assert (mr != null, "Map.Generate: There needs to be a mesh renderer on this gameobject", this);

		int actualSeed = seed;

		if (seed < 0) actualSeed = Random.Range (int.MinValue, int.MaxValue);

		System.Random rnd = new System.Random (actualSeed);

		List<Vector2> outlinePoints = MapGeneration.GenerateMapOutline (size, gridSize, controlPointScaleFactor, minOutlinePoints, actualSeed);
		mapBounds = new MapBounds (outlinePoints);

		mf.sharedMesh = MapGeneration.GenerateMapMesh (outlinePoints);

		Queue<Vector2> open = new Queue<Vector2> ();
		List<Vector2> points = new List<Vector2> ();

		Vector2 initial = new Vector2 (((float)rnd.NextDouble () * 4f) - 2f, ((float)rnd.NextDouble () * 4f) - 2f);
		open.Enqueue (initial);
		points.Add (initial);

		Vector2 current, test;

		float sqareSpread = resourceSpread * resourceSpread;
		bool isValid;

		while (open.Count > 0)
		{
			current = open.Dequeue ();

			for (int i = 0; i < 20; i++)
			{
				test = current + (Vector2)(Quaternion.Euler (0, 0, 360f * (float)rnd.NextDouble ()) * new Vector2 (resourceSpread * (2.001f + 0.1f * (float)rnd.NextDouble ()), 0));
				isValid = true;
					
				if (OnMap (test))
				{
					for (int a = 0; a < points.Count; a++)
					{
						if (Vector2.SqrMagnitude (test - points[a]) < sqareSpread)
						{
							isValid = false;
							break;
						}
					}

					if (isValid)
					{
						open.Enqueue (test);
						points.Add (test);
					}
				}
			}
		}

		//while (points.Count > numPoints) points.RemoveAt (rnd.Next (points.Count));

		List<ItemType> ores = DataManager.GetOreItems ();

		for (int i = 0; i < points.Count; i++) AddResourceArea (ores[rnd.Next (ores.Count)].name, points[i], new Vector2 (rnd.Next (3, 6), rnd.Next (3, 6)));

		BuildingType spacePortBuilding = DataManager.GetBuildingType ("SpacePort");
		if (spacePortBuilding == null) throw new System.Exception ("Map.Generate: Counld't find space port building");
		if (!TryPlaceBuilding (Vector2.zero, spacePortBuilding)) throw new System.Exception ("Map.Generate: Couldn't place space port");

		Debug.Log ("Map generated with seed: " + actualSeed);
	}

	public void AddResourceArea (string type, Vector2 point, Vector2 size)
	{
		float tileSize = resourceIconSprite.rect.width / resourceIconSprite.pixelsPerUnit;
		Vector3 actualPos = new Vector3 (Mathf.Floor (point.x / tileSize) * tileSize + (0.5f * ((int)size.x % 2)), (Mathf.Floor (point.y / tileSize) * tileSize) + (0.5f * ((int)size.y % 2)), resourceZValie);

		if (OnMap (actualPos, size.magnitude / 2f)) 
		{

			GameObject go = new GameObject ("ResourceArea");
			go.transform.SetParent (this.transform);
			go.transform.position = actualPos;
			go.transform.rotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;

			ResourceSpawn resource = go.AddComponent<ResourceSpawn> ();
			resource.Setup (type, resourceDisplayMaterial, resourceIconSprite, size);

			resourceAreas.Add (resource);
		}
	}

	public void DestroyBuilding (Building building)
	{
		List<TransportLine> toRemove = new List<TransportLine> ();


		foreach (TransportLine item in building.transportInterface.Outputs) toRemove.Add (item);
		foreach (TransportLine item in placedTransportLines) if (item.end == (ITransportConnectable)building) toRemove.Add (item);
		foreach (TransportLine item in toRemove) DestroyTransportLine (item);

		if (placedBuildings.Contains (building)) placedBuildings.Remove (building);

		Destroy (building.gameObject);

		bank.SellBuilding (building.type);
	}

	public void DestroyTransportLine (TransportLine line)
	{
		line.DestroyLine (this, 0);
	}

	public void RemoveTransportLine (TransportLine line)
	{
		bank.SellTransportLine (line);
		if (placedTransportLines.Contains (line)) placedTransportLines.Remove (line);
	}

	/// <summary>
	/// Gets a building where the point is within the collision circle of the buiding
	/// </summary>
	/// <param name="point"></param>
	/// <returns></returns>
	public Building GetBuilding (Vector2 point)
	{
		Collider2D[] result = new Collider2D[1];

		if (Physics2D.OverlapPoint (point, collisionFilter, result) > 0) return result[0].GetComponent<Building> ();
		return null;
	}

	public TransportLine GetTransportLine (Vector2 point)
	{
		Collider2D[] result = new Collider2D[1];

		if (Physics2D.OverlapPoint (point, transportCollisionFilter, result) > 0) return result[0].GetComponent<TransportLine> ();
		return null;
	}

	public ResourceSpawn GetResourceArea (Vector2 point)
	{
		return resourceAreas.Find (x => x.Contains (point));
	}

	public bool OnMap (Vector2 point, float size = 0)
	{
		if (mapBounds == null) return false;
		return mapBounds.Contains (point, size);
	}

	public bool CanPlaceBuilding (Vector2 point, BuildingType type)
	{
		return type.onBuildTest (this, point, type) && bank.CanSpend (type.cost);
	}

	public bool TryPlaceBuilding (Vector2 point, BuildingType type)
	{
		if (!CanPlaceBuilding (point, type)) return false;
		if (!bank.TrySpend (type.cost)) return false;

		GameObject go = new GameObject ("Building_" + type.name);
		go.transform.SetParent (this.transform);
		go.transform.position = new Vector3 (point.x, point.y, buildingZValue);
		go.transform.rotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;
		go.layer = DataManager.BuildingLayer;

		ShapeRenderer shape = go.AddComponent<ShapeRenderer> ();
		shape.material = buildingMaterial;
		shape.colour = needRecipeBuildingColour;
		type.SetupShapeRenderer (shape);

		PolygonCollider2D polygonCollider = go.AddComponent<PolygonCollider2D> ();
		type.SetupPolygonCollider (polygonCollider);
		polygonCollider.isTrigger = true;

		Building building = go.AddComponent<Building> ();
		building.Setup (this, type, polygonCollider);

		placedBuildings.Add (building);

		return true;
	}

	public bool TryBuildOutputLine (Building start, Building target)
	{
		// Can't build a output line if the target doesn't need any inputs as it has no recipe unless it has internal storage
		if (!target.type.needsRecipe && target.type.internalStorageSize == 0) return false;

		if (start == null) throw new System.ArgumentNullException ("Map.TryBuildOutputLine: Start building is null");
		if (target == null) throw new System.ArgumentNullException ("Map.TryBuildOutputLine: Target building is null");

		List<Vector2> points;
		List<TransportLine> overlapping;
		uint cost;

		if (CanBuildOutputLine (start, target, out points, out overlapping, out cost) && bank.TrySpend (cost))
		{
			GameObject go = new GameObject ("TransportLine");
			go.transform.SetParent (this.transform);
			go.transform.position = new Vector3 (0, 0, transportLineZValue);
			go.transform.rotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
			go.layer = DataManager.TransportLayer;

			LineRenderer2D line = go.AddComponent<LineRenderer2D> ();
			line.material = lineMaterial;
			line.width = lineWidth;

			GameObject highlight = new GameObject ("Highlight");
			highlight.transform.SetParent (go.transform);
			highlight.transform.localPosition = new Vector3 (0, 0, 0.1f);
			highlight.transform.rotation = Quaternion.identity;
			highlight.transform.localScale = Vector3.one;

			LineRenderer2D lineHighlight = highlight.AddComponent<LineRenderer2D> ();
			lineHighlight.material = lineHighlightMaterial;
			lineHighlight.width = lineHighlightWidth;

			/*PolygonCollider2D pc = */go.AddComponent<PolygonCollider2D> ();

			TransportLine transportLine = go.AddComponent<TransportLine> ();

			if (overlapping.Count > 0)
			{
				int furthestStart = 0;
				int closestEnd = points.Count - 1;

				TransportLine startLine = null;
				TransportLine endLine = null;

				foreach (TransportLine item in overlapping)
				{
					if (item.start == (ITransportConnectable)start)
					{
						for (int i = furthestStart + 1; i < points.Count; i++)
						{
							if (item.HasPoint (points[i]))
							{
								furthestStart = i;
								startLine = item;
							}
						}
					}
					else if (item.end == (ITransportConnectable)target)
					{
						for (int i = closestEnd - 1; i >= 0; i--)
						{
							if (item.HasPoint (points[i]))
							{
								closestEnd = i;
								endLine = item;
							}
						}
					}
					else
					{
						throw new System.Exception ("Map.TryBuildOutputLine: Overlapping line is not connected to the start or end of this one");
					}
				}

				ITransportConnectable newStart = start;
				ITransportConnectable newTarget = target;

				if (furthestStart != 0 && startLine != null)
				{
					if (!startLine.IsEndPoint (points[furthestStart]))
					{
						TransportLineMidpoint m = go.AddComponent<TransportLineMidpoint> ();
						m.Setup (points[furthestStart], startLine, this);
						newStart = m;
					}
					else
					{
						Debug.Log ("This would have been a split at the first or last of a line");
					}
				}

				if (closestEnd != points.Count - 1 && endLine != null)
				{
					if (!endLine.IsEndPoint (points[closestEnd]))
					{
						TransportLineMidpoint m = go.AddComponent<TransportLineMidpoint> ();
						m.Setup (points[closestEnd], endLine, this);
						newTarget = m;
					}
					else
					{
						Debug.Log ("This would have been a split at the first or last of a line");
					}
				}

				List<Vector2> actualPoints = points.GetRange (furthestStart, closestEnd + 1 - furthestStart);

				SetupLineRenderer (line, lineHighlight, newStart, newTarget, transportLine, actualPoints);
				SetupLineRenderer (lineHighlight, actualPoints);
			}
			else
			{
				SetupLineRenderer (line, lineHighlight, start, target, transportLine, points);
				SetupLineRenderer (lineHighlight, points);
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	public bool CanBuildOutputLine (Building start, Building target, out List<Vector2> points, out List<TransportLine> overlapping, out uint cost)
	{
		overlapping = new List<TransportLine> ();
		
		// Get the initial set of points
		points = GetTransportLinePoints (start.position, target.position, start.type.size, target.type.size);
		cost = Bank.GetNewTransportLineCost (points);

		if (!bank.CanSpend (cost)) return false;

		if (!target.type.needsRecipe && target.type.internalStorageSize == 0) return false;

		// Make sure that these don't overlap with a building or other lines
		currentBuildingOutputlineCollider.points = LineGeneration.Outline (points, lineWidth).ToArray ();

		for (int i = 0; i < placedBuildings.Count; i++)
			if (placedBuildings[i] != start && placedBuildings[i] != target)
				if (currentBuildingOutputlineCollider.Distance (placedBuildings[i].polygonCollider).isOverlapped) return false;

		ColliderDistance2D distance;

		for (int i = 0; i < placedTransportLines.Count; i++)
		{
			if (placedTransportLines[i].start != (ITransportConnectable)start && placedTransportLines[i].end != (ITransportConnectable)target)
			{
				if ((distance = currentBuildingOutputlineCollider.Distance (placedTransportLines[i].polygonCollider)).isOverlapped)
				{
					if (placedTransportLines[i].start != (ITransportConnectable)target)
					{
						if (placedTransportLines[i].end != (ITransportConnectable)start) return false;
						else if (Mathf.Abs (distance.distance) > start.type.size) return false;
					}
					else
					{
						if (Mathf.Abs (distance.distance) > target.type.size) return false;
					}
				}
			}
			else
			{
				overlapping.Add (placedTransportLines[i]);
			}
		}

		return true;
	}

	public void SetupLineRenderer (LineRenderer2D line, LineRenderer2D outline, ITransportConnectable start, ITransportConnectable end, TransportLine transportLine, List<Vector2> points)
	{
		if (line == null) throw new System.ArgumentNullException ("Map.SetupLineRenderer: Line is null");

		// Actual do the setup
		SetupLineRenderer (line, points);

		// Setup the transport line if it is given
		if (transportLine != null)
		{
			transportLine.Setup (start, end, line.points, transportSpeed, lineWidth, line, outline);

			if (!placedTransportLines.Contains (transportLine)) placedTransportLines.Add (transportLine);
		}
	}

	public void SetupLineRenderer (LineRenderer2D line, List<Vector2> points)
	{
		if (line == null) throw new System.ArgumentNullException ("Map.SetupLineRenderer: Line is null");

		line.points = points;
		line.Regenerate ();
		line.gameObject.SetActive (true);
	}

	public List<Vector2> GetTransportLinePoints (Vector2 startPosition, Vector2 endPosition, float startSize, float endSize, bool snapEndToGrid = true)
	{
		Vector2 delta = endPosition - startPosition;
		Vector2 snappedStart = startPosition;
		Vector2 snappedEnd = endPosition;

		snappedStart /= gridSize;
		snappedEnd /= gridSize;

		if (endPosition.x > startPosition.x)
		{
			if (endPosition.y > startPosition.y)
			{
				snappedStart = new Vector2 (Mathf.CeilToInt (snappedStart.x), Mathf.CeilToInt (snappedStart.y));
				snappedEnd = new Vector2 (Mathf.FloorToInt (snappedEnd.x), Mathf.FloorToInt (snappedEnd.y));
			}
			else
			{
				snappedStart = new Vector2 (Mathf.CeilToInt (snappedStart.x), Mathf.FloorToInt (snappedStart.y));
				snappedEnd = new Vector2 (Mathf.FloorToInt (snappedEnd.x), Mathf.CeilToInt (snappedEnd.y));
			}
		}
		else
		{
			if (endPosition.y > startPosition.y)
			{
				snappedStart = new Vector2 (Mathf.FloorToInt (snappedStart.x), Mathf.CeilToInt (snappedStart.y));
				snappedEnd = new Vector2 (Mathf.CeilToInt (snappedEnd.x), Mathf.FloorToInt (snappedEnd.y));
			}
			else
			{
				snappedStart = new Vector2 (Mathf.FloorToInt (snappedStart.x), Mathf.FloorToInt (snappedStart.y));
				snappedEnd = new Vector2 (Mathf.CeilToInt (snappedEnd.x), Mathf.CeilToInt (snappedEnd.y));
			}
		}

		snappedStart *= gridSize;
		snappedEnd *= gridSize;

		float minDelta =  gridSize * 2;

		List<Vector2> points = new List<Vector2> ();

		Vector2 midPoint = Vector2.zero;

		if (Mathf.Abs (delta.x) > minDelta || Mathf.Abs (delta.y) > minDelta)
		{
			points.Add (startPosition + ((snappedStart - startPosition).normalized * startSize * 0.5f));

			if (Mathf.Abs (delta.x) > minDelta)
			{
				if (Mathf.Abs (delta.y) > minDelta)
				{
					if (Mathf.Abs (delta.x) > Mathf.Abs (delta.y))
					{
						midPoint = new Vector2 (snappedEnd.x, snappedStart.y);

						if (Vector2.Distance (startPosition, snappedStart) > startSize) points.Add (snappedStart);
						else if (Vector2.Distance (snappedStart, midPoint) > startSize) points.Add (SnapToGrid (snappedStart + ((midPoint - snappedStart).normalized * startSize)));

						points.Add (SnapToGrid (midPoint + ((snappedStart - midPoint).normalized * 0.5f)));
						points.Add (SnapToGrid (midPoint + ((snappedEnd - midPoint).normalized * 0.5f)));
					}
					else
					{
						midPoint = new Vector2 (snappedStart.x, snappedEnd.y);

						if (Vector2.Distance (startPosition, snappedStart) > startSize) points.Add (snappedStart);
						else if (Vector2.Distance (snappedStart, midPoint) > startSize) points.Add (SnapToGrid (snappedStart + ((midPoint - snappedStart).normalized * startSize)));

						points.Add (SnapToGrid (midPoint + ((snappedStart - midPoint).normalized * 0.5f)));
						points.Add (SnapToGrid (midPoint + ((snappedEnd - midPoint).normalized * 0.5f)));
					}
				}
				else
				{
					midPoint = SnapToGrid (new Vector2 (snappedEnd.x, snappedStart.y));

					//if (Vector2.Distance (startPosition, snappedStart) > startSize) points.Add (snappedStart);
					//else if (Vector2.Distance (snappedStart, midPoint )> startSize) points.Add (SnapToGrid (snappedStart + ((midPoint - snappedStart).normalized * startSize)));

					points.Add (midPoint);
				}
			}
			else
			{
				midPoint = SnapToGrid (new Vector2 (snappedStart.x, snappedEnd.y));

				//if (Vector2.Distance (startPosition, snappedStart) > startSize) points.Add (snappedStart);
				//else if (Vector2.Distance (snappedStart, midPoint) > startSize) points.Add (SnapToGrid (snappedStart + ((midPoint - snappedStart).normalized * startSize)));

				points.Add (midPoint);
			}

			if (snapEndToGrid)
			{
				if (Vector2.Distance (endPosition, snappedEnd) > endSize) points.Add (snappedEnd);
				else if (Vector2.Distance (snappedEnd, midPoint) > endSize) points.Add (SnapToGrid (snappedEnd + ((midPoint - snappedEnd).normalized * endSize)));
			}

			points.Add (endPosition + ((snappedEnd - endPosition).normalized * endSize * 0.5f));
		}
		else
		{
			points.Add (startPosition + ((endPosition - startPosition).normalized * startSize * 0.5f));
			points.Add (endPosition + ((startPosition - endPosition).normalized * endSize* 0.5f));
		}

		return GetMovementControlPoints (points);
	}

	public List<Vector2> GetMovementControlPoints (List<Vector2> points)
	{
		List<Vector2> controlPoints = new List<Vector2> ();
		controlPoints.Add (points[0]);

		Vector2 direction;
		float distance;

		for (int i = 1; i < points.Count - 2; i++)
		{
			direction = points[i + 1] - points[i];
			distance = direction.magnitude;

			if (distance > gridSize)
			{
				direction = direction.normalized;

				for (float d = 0; d < distance; d += gridSize) controlPoints.Add (points[i] + direction * d);
			}
		}

		controlPoints.Add (points[points.Count - 1]);

		return controlPoints;
	}

	public Vector2 SnapToGrid (Vector2 point)
	{
		return new Vector2 (Mathf.Floor (point.x / gridSize), Mathf.Floor (point.y / gridSize)) * gridSize;
	}

	public bool IsBuildCheckCollision ()
	{
		Collider2D[] result = new Collider2D[1];

		if (Physics2D.OverlapCollider (currentBuildingTypeCollider, buildCollisionFilter, result) > 0) return true;
		return false;
	}

	private void OnDrawGizmosSelected ()
	{
		if (mapBounds != null)
		{
			mapBounds.DrawnBoundingBoxesGizmos ();
		}

		//if (mf != null)
		//{
		//	if (mf.sharedMesh != null)
		//	{
		//		Gizmos.color = Color.black;

		//		for (int i = 0; i < mf.sharedMesh.vertices.Length; i++) Gizmos.DrawSphere (mf.sharedMesh.vertices[i], 0.1f);
		//	}
		//}
	}
}

#if UNITY_EDITOR
[CustomEditor (typeof (Map))]
public class MapEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		Map map = (Map)target;

		if (GUILayout.Button ("Generate")) map.Generate ();
	}
}
#endif
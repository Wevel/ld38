﻿using System.Collections.Generic;
using UnityEngine;

public class MapBounds
{
	private List<Vector2> topPoints;
	private List<Vector2> bottomPoints;

	private float insideLeft;
	private float insideRight;
	private float insideTop;
	private float insideBottom;

	private float outsideLeft;
	private float outsideRight;
	private float outsideTop;
	private float outsideBottom;

	public MapBounds (List<Vector2> outline)
	{
		topPoints = new List<Vector2> ();
		bottomPoints = new List<Vector2> ();

		insideLeft = float.NegativeInfinity;
		insideRight = float.PositiveInfinity;
		insideTop = float.PositiveInfinity;
		insideBottom = float.NegativeInfinity;

		outsideLeft = 0;
		outsideRight = 0;
		outsideTop = 0;
		outsideBottom = 0;

		#region Setup

		for (int i = 0; i < outline.Count; i++)
		{
			if (outline[i].x > 0)
			{
				if (outline[i].y > 0)
				{
					topPoints.Add (outline[i]);

					if (Mathf.Abs (outline[i].x) >= Mathf.Abs (outline[i].y) && outline[i].x < insideRight) insideRight = outline[i].x;
					if (Mathf.Abs (outline[i].x) <= Mathf.Abs (outline[i].y) && outline[i].y < insideTop) insideTop = outline[i].y;

					if (outline[i].x > outsideRight) outsideRight = outline[i].x;
					if (outline[i].y > outsideTop) outsideTop = outline[i].y;
				}
				else
				{
					bottomPoints.Add (outline[i]);

					if (Mathf.Abs (outline[i].x) >= Mathf.Abs (outline[i].y) && outline[i].x < insideRight) insideRight = outline[i].x;
					if (Mathf.Abs (outline[i].x) <= Mathf.Abs (outline[i].y) && outline[i].y > insideBottom) insideBottom = outline[i].y;

					if (outline[i].x > outsideRight) outsideRight = outline[i].x;
					if (outline[i].y < outsideBottom) outsideBottom = outline[i].y;
				}
			}
			else
			{
				if (outline[i].y > 0)
				{
					topPoints.Add (outline[i]);

					if (Mathf.Abs (outline[i].x) >= Mathf.Abs (outline[i].y) && outline[i].x > insideLeft) insideLeft = outline[i].x;
					if (Mathf.Abs (outline[i].x) <= Mathf.Abs (outline[i].y) && outline[i].y < insideTop) insideTop = outline[i].y;

					if (outline[i].x < outsideLeft) outsideLeft = outline[i].x;
					if (outline[i].y > outsideTop) outsideTop = outline[i].y;
				}
				else
				{
					bottomPoints.Add (outline[i]);

					if (Mathf.Abs (outline[i].x) >= Mathf.Abs (outline[i].y) && outline[i].x > insideLeft) insideLeft = outline[i].x;
					if (Mathf.Abs (outline[i].x) <= Mathf.Abs (outline[i].y) && outline[i].y > insideBottom) insideBottom = outline[i].y;

					if (outline[i].x < outsideLeft) outsideLeft = outline[i].x;
					if (outline[i].y < outsideBottom) outsideBottom = outline[i].y;
				}
			}
		}

		insideLeft++;
		insideRight--;
		insideTop--;
		insideBottom++;

		topPoints.Sort ((a, b) => a.x.CompareTo (b.x));
		bottomPoints.Sort ((a, b) => a.x.CompareTo (b.x));

		#endregion
	}

	public bool Contains (Vector2 point, float size)
	{
		// Make sure the size is a positive number
		size = Mathf.Abs (size);

		// Can perform some much faster checks that will work for most points and are much faster
		// If neither is the case, then need to perform an actual check
		// Check if the point is inside the inner bounding box, if it is then the map contains the point
		if (point.x + size < insideRight && point.x - size > insideLeft && point.y + size < insideTop && point.y - size > insideBottom) return true;

		// Check if the point is outside the outer bounding box, if it is then the map doesnt contain the point
		if (point.x + size > outsideRight || point.x - size < outsideLeft || point.y + size > outsideTop || point.y - size < outsideBottom) return false;

		// Otherwise, need to check the quaderant to see if it is valid
		float sizeSqare = size * size;

		Vector2 p0, p1;

		if (point.y > 0)
		{
			if (topPoints.Count > 1)
			{
				p0 = topPoints[0];
				p1 = topPoints[topPoints.Count - 1];

				for (int i = 0; i < topPoints.Count; i++)
				{
					if (topPoints[i].x < point.x)
					{
						if (topPoints[i].x > p0.x) p0 = topPoints[i];
					}
					else
					{
						if (topPoints[i].x < p1.x)
						{
							p1 = topPoints[i];
							break;
						}
					}
				}

				return point.y + size < ((p1.y - p0.y) / (p1.x - p0.x)) * (point.x - p0.x) + p0.y;
			}
			else
			{
				return point.x < topPoints[0].x && point.y < topPoints[0].y && Vector2.SqrMagnitude (point - topPoints[0]) < sizeSqare;
			}
		}
		else
		{
			if (bottomPoints.Count > 1)
			{
				p0 = bottomPoints[0];
				p1 = bottomPoints[bottomPoints.Count - 1];

				for (int i = 0; i < bottomPoints.Count; i++)
				{
					if (bottomPoints[i].x < point.x)
					{
						if (bottomPoints[i].x > p0.x) p0 = bottomPoints[i];
					}
					else
					{
						if (bottomPoints[i].x < p1.x)
						{
							p1 = bottomPoints[i];
							break;
						}
					}
				}

				return point.y - size > ((p1.y - p0.y) / (p1.x - p0.x)) * (point.x - p0.x) + p0.y;
			}
			else
			{
				return point.x < bottomPoints[0].x && point.y < bottomPoints[0].y && Vector2.SqrMagnitude (point - bottomPoints[0]) < sizeSqare;
			}
		}
	}

	public void DrawnBoundingBoxesGizmos ()
	{
		Gizmos.color = Color.blue;

		Gizmos.DrawLine (new Vector3 (insideLeft, insideBottom), new Vector3 (insideLeft, insideTop));
		Gizmos.DrawLine (new Vector3 (insideLeft, insideTop), new Vector3 (insideRight, insideTop));
		Gizmos.DrawLine (new Vector3 (insideRight, insideTop), new Vector3 (insideRight, insideBottom));
		Gizmos.DrawLine (new Vector3 (insideRight, insideBottom), new Vector3 (insideLeft, insideBottom));

		Gizmos.color = Color.red;

		Gizmos.DrawLine (new Vector3 (outsideLeft, outsideBottom), new Vector3 (outsideLeft, outsideTop));
		Gizmos.DrawLine (new Vector3 (outsideLeft, outsideTop), new Vector3 (outsideRight, outsideTop));
		Gizmos.DrawLine (new Vector3 (outsideRight, outsideTop), new Vector3 (outsideRight, outsideBottom));
		Gizmos.DrawLine (new Vector3 (outsideRight, outsideBottom), new Vector3 (outsideLeft, outsideBottom));
	}
}

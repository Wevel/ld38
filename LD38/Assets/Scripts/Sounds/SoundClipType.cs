﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundClipType
{
	public string name;
	public List<AudioClip> clip = new List<AudioClip> ();
}

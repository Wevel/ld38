﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType
{
	Mine,
	Craft,
	Buy,
	Sell
}

public class Sound : MonoBehaviour
{
	public static Sound Instance { get; private set; }

	public static void PlaySound (string name, SoundType type)
	{
		Instance.playSound (name, type);
	}

	public AudioSource audioSourcePrefab;

	public SoundClipType mineSoundTypes;
	public SoundClipType createSoundTypes;
	public SoundClipType buySoundTypes;
	public SoundClipType sellSoundTypes;

	//public List<SoundClipType> mineSoundTypes = new List<SoundClipType> ();
	//public List<SoundClipType> createSoundTypes = new List<SoundClipType> ();
	//public List<SoundClipType> buySoundTypes = new List<SoundClipType> ();
	//public List<SoundClipType> sellSoundTypes = new List<SoundClipType> ();

	private Queue<AudioSource> unusedSources = new Queue<AudioSource> ();

	private float lastTimer = 0;

	private void Awake ()
	{
		Instance = this;
	}

	private void Update ()
	{
		if (lastTimer > 0) lastTimer -= Time.deltaTime;
	}

	public void playSound (string name, SoundType type)
	{
		if (lastTimer > 0) return;

		SoundClipType clipType = null;

		switch (type)
		{
			case SoundType.Mine:
				clipType = mineSoundTypes;//.Find (x => x.name == name);
				break;
			case SoundType.Craft:
				clipType = createSoundTypes;//.Find (x => x.name == name);
				break;
			case SoundType.Buy:
				clipType = buySoundTypes;//.Find (x => x.name == name);
				break;
			case SoundType.Sell:
				clipType = sellSoundTypes;//.Find (x => x.name == name);
				break;
			default:
				break;
		}

		if (clipType != null)
		{
			if (clipType.clip.Count > 0) StartCoroutine (runSound (clipType.clip[Random.Range (0, clipType.clip.Count)]));
		}
	}

	private IEnumerator runSound (AudioClip clip)
	{
		lastTimer = clip.length / 2f;
		AudioSource source = getSource ();
		source.gameObject.SetActive (true);
		source.clip = clip;
		source.Play ();

		while (source.isPlaying) yield return new WaitForEndOfFrame ();

		finishSourceUse (source);
	}

	private AudioSource getSource ()
	{
		if (unusedSources.Count > 0)
		{
			return unusedSources.Dequeue ();
		}

		AudioSource source = Instantiate (audioSourcePrefab);
		source.transform.SetParent (transform);

		return source;
	}

	private void finishSourceUse (AudioSource source)
	{
		source.gameObject.SetActive (false);
		unusedSources.Enqueue (source);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LineRenderer2D : MonoBehaviour
{
	public float width = 0.1f;
	public List<Vector2> points;

	public Material material;

	private MeshFilter mf;
	private MeshRenderer mr;

	private float oldWidth;
	private List<Vector2> oldPoints;

	private void Awake ()
	{
		setup ();
	}

	private void OnEnable ()
	{
		if (mf == null || mr == null) setup ();

		Regenerate ();

		mr.enabled = true;
	}

	private void OnDisable ()
	{
		mr.enabled = false;
	}

	private void Update ()
	{
		if (mf == null || mr == null) setup ();

		mr.sharedMaterial = material;

		if (oldWidth != width || oldPoints.Count != points.Count)
		{
			Regenerate ();
		}
		else
		{
			for (int i = 0; i < points.Count; i++)
			{
				if (points[i] != oldPoints[i])
				{
					Regenerate ();
					break;
				}
			}
		}
	}

	private void OnDestroy ()
	{
		if (mr != null && mr.material != null) Destroy (mr.material);
		if (mf != null && mf.mesh != null) Destroy (mf.mesh);
	}

	public void Regenerate ()
	{
		if (mf == null || mr == null) setup ();

		if (width < 0) width = 0.01f;

		if (mf != null && mf.mesh != null) Destroy (mf.mesh);

		mf.mesh = LineGeneration.GenerateLineMesh (points, width);

		oldWidth = width;
		if (points != null) oldPoints = new List<Vector2> (points);
		else oldPoints = null;
	}

	private void setup ()
	{
		mr = this.GetComponent<MeshRenderer> ();
		mf = this.GetComponent<MeshFilter> ();
		if (mr == null) mr = gameObject.AddComponent<MeshRenderer> ();
		if (mf == null) mf = gameObject.AddComponent<MeshFilter> ();

		mr.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
		mf.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
	}

	private void OnDrawGizmosSelected ()
	{
		if (mf != null && mf.sharedMesh != null)
		{
			Gizmos.color = Color.black;
			Gizmos.DrawWireMesh (mf.sharedMesh);
		}
	}
}

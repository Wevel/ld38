﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

//[ExecuteInEditMode]
public class ShapeRenderer : MonoBehaviour {

	public Color colour = Color.white;
	public float size = 0.5f;
	public int sides = 4;
	public ShapeMeshType type = ShapeMeshType.Basic;

	public bool outline = false;

	[HideInInspector]
	public float wallPercentThickness = 0.5f;

	public Material material;

	MeshFilter mf;
	MeshRenderer mr;

	private float oldSize;
	private int oldSides;
	private ShapeMeshType oldType;
	private bool oldOutline;
	private float oldWallPercentThickness;
	private Material oldMaterial;
	private Color oldColour;

	private void OnEnable ()
	{
		checkComonents ();

		mr.enabled = true;
	}

	private void OnDisable ()
	{
		checkComonents ();

		mr.enabled = false;
	}

	void Update () {
		checkComonents ();

		if (oldMaterial != material || oldColour != colour)
		{
			if (mr.material != null) Destroy (mr.material);

			mr.material = material;
			mr.material.color = colour;

			oldMaterial = material;
			oldColour = colour;
		}

		if (oldSize != size || oldSides != sides || oldType != type || oldOutline != outline || oldWallPercentThickness != wallPercentThickness) Regenerate ();
	}

	private void OnDestroy ()
	{
		if (mr != null && mr.material != null) Destroy (mr.material);
		if (mf != null && mf.mesh != null) Destroy (mf.mesh);
	}

	public void Regenerate ()
	{
		checkComonents ();

		if (sides < 3) sides = 3;
		if (size < 0) size = 0;

		if (mf != null && mf.mesh != null) Destroy (mf.mesh);

		if (outline) mf.mesh = ShapeGeneration.GenerateShapeMesh (size, sides, 90f - (180f / sides), type, wallPercentThickness);
		else mf.mesh = ShapeGeneration.GenerateShapeMesh (size, sides, 90f - (180f / sides), type);

		oldSize = size;
		oldSides = sides;
		oldType = type;
		oldOutline = outline;
		oldWallPercentThickness = wallPercentThickness;
	}

	private void checkComonents ()
	{
		if (mf == null)
		{
			mf = this.GetComponent<MeshFilter> ();
			if (mf == null) mf = gameObject.AddComponent<MeshFilter> ();
			mf.hideFlags = HideFlags.HideInInspector;
		}

		if (mr == null)
		{
			mr = this.GetComponent<MeshRenderer> ();
			if (mr == null) mr = gameObject.AddComponent<MeshRenderer> ();
			mr.hideFlags = HideFlags.HideInInspector;
		}
	}

	//private void OnDrawGizmosSelected ()
	//{
	//	if (mf != null)
	//	{
	//		if (mf.sharedMesh != null)
	//		{
	//			Gizmos.matrix = transform.localToWorldMatrix;
	//			Gizmos.color = Color.black;
	//			Gizmos.DrawWireMesh (mf.sharedMesh);
	//		}
	//	}
	//}
}

#if UNITY_EDITOR
[CustomEditor (typeof (ShapeRenderer))]
public class ShapeTestEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		ShapeRenderer shape = (ShapeRenderer)target;

		if (shape.outline)
		{
			float oldPercent = shape.wallPercentThickness;
			shape.wallPercentThickness = Mathf.Clamp (EditorGUILayout.FloatField ("Wall Percent Thickness", shape.wallPercentThickness), 0, 1);

			if (oldPercent != shape.wallPercentThickness) EditorUtility.SetDirty (shape);
		}
		
		//if (GUILayout.Button ("Regenerate")) shape.Regenerate ();
	}
}
#endif